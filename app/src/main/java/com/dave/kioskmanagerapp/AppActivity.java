package com.dave.kioskmanagerapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.LayoutInflaterCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.dave.kioskmanagerapp.fragments.ImageFragment;
import com.dave.kioskmanagerapp.fragments.MarqueeFragment;
import com.dave.kioskmanagerapp.fragments.MultiMediaFragment;
import com.dave.kioskmanagerapp.fragments.VideoFragment;
import com.dave.kioskmanagerapp.fragments.VideoStreamerFragment;
import com.dave.kioskmanagerapp.fragments.WebviewFragment;
import com.dave.kioskmanagerapp.motiondetection.MotionDetector;
import com.dave.kioskmanagerapp.motiondetection.MotionDetectorCallback;
import com.dave.kioskmanagerapp.ocv_detection.FaceDetector;
import com.dave.kioskmanagerapp.ocv_detection.listeners.OnFaceDetectedListener;
import com.dave.kioskmanagerapp.utils.BroadcastNames;
import com.dave.kioskmanagerapp.utils.FragmentMapObject;
import com.dave.kioskmanagerapp.utils.StickyImmersiveFullScreen;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;
import org.opencv.core.Rect;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class AppActivity extends FragmentActivity {
    private MotionDetector motionDetector;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        StickyImmersiveFullScreen.getFullScreen(getWindow());


        try {

            String appData = /*vertical_two_apps;*/getIntent().getExtras().getString("app_data");
            JSONObject app_params = new JSONObject(appData);
            LinearLayout parentView = (LinearLayout)findViewById(R.id.parentView);
            String split_dir = null;
            if(app_params.has("split_dir")) {
                split_dir = app_params.getString("split_dir");

                if(split_dir.equals("vertical")){
                    parentView.setOrientation(LinearLayout.VERTICAL);
                }else{
                    parentView.setOrientation(LinearLayout.HORIZONTAL);
                }
            }

            (parentView).addView(addFrame(app_params));
            for (String key : fragentMap.keySet()) {
                FragmentMapObject fragmentMapObject = fragentMap.get(key);

                Bundle bundle = new Bundle();
                bundle.putString("app_suffix", key);
                bundle.putString("app_id", fragmentMapObject.app_id);
                bundle.putString("app_device_id", fragmentMapObject.app_device_id);
                if(fragmentMapObject.params.size()>0){
                    for (String key1 : fragmentMapObject.params.keySet()){
                        bundle.putString(key1,fragmentMapObject.params.get(key1));
                    }
                }
                fragmentMapObject.fragment.setArguments(bundle);
                addFragment(fragmentMapObject.frameId,fragmentMapObject.fragment);
            }

            motionDetector = new MotionDetector(this, (SurfaceView) findViewById(R.id.surfaceView));
            motionDetector.setMotionDetectorCallback(new MotionDetectorCallback() {
                @Override
                public void onMotionDetected(Bitmap img, boolean faceAlreadyDetected) {
                    try {
                        ((ImageView) AppActivity.this.findViewById(R.id.imgFound)).setImageBitmap(img);

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if(!faceAlreadyDetected) {
                        sendMyBroadCast(BroadcastNames.MOTION_DETECTED.name(), "", null, null);
                    }
                    FaceDetector faceDetector = new FaceDetector();
                    faceDetector.processImage(img, AppActivity.this, new OnFaceDetectedListener() {
                        @Override
                        public void onFaceDetected(Rect[] faces) {
                            if (faces.length > 0) {
                                motionDetector.setFaceAvailbleAlready(true);

                                //OPEN to see camera face rect output
                                /*Canvas canvas = new Canvas(img);
                                Paint paint = new Paint();
                                paint.setStyle(Paint.Style.STROKE);
                                paint.setColor(Color.YELLOW);
                                paint.setAntiAlias(true);
                                for(int i = 0 ; i < faces.length ; i++) {
                                    android.graphics.Rect rect = new android.graphics.Rect(faces[i].x, faces[i].y, faces[i].x + faces[i].width, faces[i].y + faces[i].height);
                                    canvas.drawRect(rect, paint);
                                }*/
                                sendMyBroadCast(BroadcastNames.FACE_DETECTED.name(), "",img, faces);

                            } else {
                                motionDetector.setFaceAvailbleAlready(false);
                            }
                        }

                        @Override
                        public void onError(String error, int errorCode) {

                        }
                    }, false);
                }

                @Override
                public void onMotionDetected() {

                }

                @Override
                public void onTooDark() {
                    Log.e("LOGGER","Too Dark To Detect Motion");
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        motionDetector.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        motionDetector.onPause();
    }

    private void addFragment(int frameId, Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.add(frameId,fragment).commit();
    }

/*

    String vertical_two_apps = "{\n" +
            "    \"level\" : 0,\n" +
            "    \"app_suffix\" : \"0\",\n" +
            "    \"x1\" : 0,\n" +
            "    \"y1\" : 0,\n" +
            "    \"x2\" : 100,\n" +
            "    \"y2\" : 100,\n" +
            "    \"app_id\" : null,\n" +
            "    \"split_dir\" : \"vertical\",\n" +
            "    \"children\" : [\n" +
            "        {    \n" +
            "            \"level\" : 1,\n" +
            "            \"app_suffix\" : \"0_0\",\n" +
            "            \"x1\" : 0,\n" +
            "            \"y1\" : 0,\n" +
            "            \"x2\" : 100,\n" +
            "            \"y2\" : 60,\n" +
            "            \"app_id\" : \"app.video.sociograph.videoapp\",\n" +
            "            \"app_type\" : \"video\",\n" +
            "            \"split_dir\" : \"vertical\",\n" +
            "            \"children\" : []\n" +
            "        },\n" +
            "        {    \n" +
            "            \"level\" : 1,\n" +
            "            \"app_suffix\" : \"0_1\",\n" +
            "            \"x1\" : 0,\n" +
            "            \"y1\" : 60,\n" +
            "            \"x2\" : 100,\n" +
            "            \"y2\" : 100,\n" +
            "            \"app_id\" : \"com.dave.sociograph.imageviewer\",\n" +
            "            \"app_type\" : \"image\",\n" +
            "            \"split_dir\" : \"vertical\",\n" +
            "            \"children\" : []\n" +
            "        }\n" +
            "    ]\n" +
            "}";

*/


    HashMap<String,FragmentMapObject> fragentMap = new HashMap<>();
    int frameCount = 0;

    private View addFrame(JSONObject app_params) {
        try {
            Log.e("LOGGER","called for : - " +app_params);
            String split_dir = "";

            if(app_params.has("split_dir")) {
                split_dir = app_params.getString("split_dir");
            }
            String app_device_id = "";
            if(app_params.has("app_device_id")) {
                app_device_id = app_params.getString("app_device_id");
            }
            String app_suffix = "0";
            if(app_params.has("app_suffix")) {
                app_suffix = app_params.getString("app_suffix");
            }
            int x1 = app_params.getInt("x1");
            int x2 = app_params.getInt("x2");
            int y1 = app_params.getInt("y1");
            int y2 = app_params.getInt("y2");
            JSONArray children = new JSONArray();

            JSONArray media_urls = new JSONArray();
            JSONObject settings = new JSONObject();

            if(app_params.has("settings")){
                settings = app_params.getJSONObject("settings");
            }

            if(app_params.has("media_urls")){
                media_urls = app_params.getJSONArray("media_urls");
            }

            if(app_params.has("children")){
                children = app_params.getJSONArray("children");
            }


            String app_id = "";
            if(app_params.has("app_id")){
                app_id = app_params.getString("app_id");
            }
            String app_type = null;
            if(app_params.has("app_type")) {
                app_type = app_params.getString("app_type");
            }

            if ((app_type == null||app_type.equals("null")|| TextUtils.isEmpty(app_type)) && children.length()>0){
                LinearLayout subParent = new LinearLayout(this);

                int height = 0;
                int width = 0;

                float weight = 100f;

                if(split_dir.equals("vertical")){
                    width = MATCH_PARENT;
                    weight = y2-y1;
                }else{
                    height = MATCH_PARENT;
                    weight = x2-x1;
                }

                LinearLayout.LayoutParams subParentParams = new LinearLayout.LayoutParams(width,height,weight);
                subParent.setLayoutParams(subParentParams);


                if(split_dir.equals("vertical")){
                    subParent.setOrientation(LinearLayout.VERTICAL);
                }else{
                    subParent.setOrientation(LinearLayout.HORIZONTAL);
                }
                subParent.setWeightSum(100f);

                for(int i = 0 ; i < children.length() ; i++){
                    View frame = addFrame(children.getJSONObject(i));
                    subParent.addView(frame);
                }
                return subParent;
            }else{
                FrameLayout frame = getChildView(x1,x2,y1,y2,split_dir);
                if(app_type.equals("video")){
                    VideoFragment vidFrag = new VideoFragment();
                    HashMap<String, String> param = new HashMap<>();
                    if(settings.length()>0) {
                        param.put("settings", settings.toString());
                    }
                    fragentMap.put(app_suffix,new FragmentMapObject(vidFrag,app_id,++frameCount,app_device_id,app_type,param));
                    frame.setId(frameCount);

                }else if(app_type.equals("image")){
                    ImageFragment imgFrag = new ImageFragment();
                    imgFrag.setApp_suffix(app_suffix);
                    HashMap<String, String> param = new HashMap<>();
                    if(settings.length()>0) {
                        param.put("settings", settings.toString());
                    }
                    fragentMap.put(app_suffix,new FragmentMapObject(imgFrag,app_id,++frameCount,app_device_id,app_type,param));
                    frame.setId(frameCount);
                }else if(app_type.equals("audio")){
                    HashMap<String, String> param = new HashMap<>();
                    if(settings.length()>0) {
                        param.put("settings", settings.toString());
                    }
                }else if(app_type.equals("marquee")){
                    MarqueeFragment vidFrag = new MarqueeFragment();
                    HashMap<String, String> param = new HashMap<>();
                    if(media_urls.length()>0) {
                        param.put("url", media_urls.toString());
                    }
                    if(settings.length()>0) {
                        param.put("settings", settings.toString());
                    }
                    fragentMap.put(app_suffix,new FragmentMapObject(vidFrag,app_id,++frameCount,app_device_id,app_type,param));
                    frame.setId(frameCount);
                }else if(app_type.equals("streaming")){
                    VideoStreamerFragment vidFrag = new VideoStreamerFragment();
                    HashMap<String, String> param = new HashMap<>();
                    if(media_urls.length()>0) {
                        param.put("url", media_urls.toString());
                    }
                    if(settings.length()>0) {
                        param.put("settings", settings.toString());
                    }
                    fragentMap.put(app_suffix,new FragmentMapObject(vidFrag,app_id,++frameCount,app_device_id,app_type,param));
                    frame.setId(frameCount);
                }else if(app_type.equals("webview")){
                    WebviewFragment vidFrag = new WebviewFragment();
                    HashMap<String, String> param = new HashMap<>();
                    if(media_urls.length()>0) {
                        param.put("url", media_urls.getString(0));
                    }
                    if(settings.length()>0) {
                        param.put("settings", settings.toString());
                    }
                    fragentMap.put(app_suffix,new FragmentMapObject(vidFrag,app_id,++frameCount,app_device_id,app_type,param));
                    frame.setId(frameCount);
                }
                return frame;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return new View(this);
    }


    private FrameLayout getChildView(int x1, int x2, int y1, int y2, String splitDirection){




        FrameLayout view = new FrameLayout(this);




        int height = 0;
        int width = 0;

        float weight = 100f;

        if(splitDirection.equals("vertical")){
            width = MATCH_PARENT;
            weight = y2-y1;
        }else{
            height = MATCH_PARENT;
            weight = x2-x1;
        }


        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width,height,weight);

        view.setLayoutParams(params);

        return view;

    }





    private void sendMyBroadCast(String broadcastId,String tags, Bitmap img, Rect[] faces)
    {
        try
        {
            Log.e("LOGGER","SENDING TAGS TO SUPER APP = "+tags);
            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(broadcastId);

            broadCastIntent.putExtra("data", tags);
            if(img != null) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                img.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();

//                img.recycle();
                broadCastIntent.putExtra("img", byteArray);
            }
            if(faces!=null){

                Gson gson = new Gson();
                String json = gson.toJson(faces);
                broadCastIntent.putExtra("faces",json);
            }
            sendBroadcast(broadCastIntent);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
