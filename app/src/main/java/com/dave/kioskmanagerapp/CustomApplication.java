package com.dave.kioskmanagerapp;

import android.app.Application;
import android.content.Context;

import com.dave.kioskmanagerapp.utils.SharedPreference;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.others.CacheModelPOJO;
import com.example.admin.daveai.others.DaveAI;


/**
 * Created by soham on 28/6/18.
 */

public class CustomApplication extends Application {

//    public static HashMap<String,ObjectArrayData> objectMap= new HashMap<String,ObjectArrayData>();
    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreference.getInstance(getApplicationContext());
        new DaveAI().initSDK(this);
        initSDK(this);
        /*
        setupLeakCanary();*/
    }
    public void initSDK(Context context) {
        DatabaseManager databaseManager = DatabaseManager.getInstance(context);
        databaseManager.createTable("", DatabaseConstants.CacheTableType.META_DATA);
        databaseManager.insertMetaData(new CacheModelPOJO("models", 604800000L, 86400000L, 0L, "model_name", 1));
        databaseManager.insertMetaData(new CacheModelPOJO("objects", 604800000L, 86400000L, 0L, "object_id", 1));
        databaseManager.insertMetaData(new CacheModelPOJO("pivots", 604800000L, 86400000L, 0L, "object_id", 1));
        databaseManager.insertMetaData(new CacheModelPOJO("preferences", 604800000L, 86400000L, 0L, "preference_id", 1));
        databaseManager.insertMetaData(new CacheModelPOJO("singletons", 604800000L, 86400000L, 0L, "model_name", 1));
        databaseManager.insertMetaData(new CacheModelPOJO("recommendations", 604800000L, 86400000L, 0L, "recommendation_id", 1));
        databaseManager.insertMetaData(new CacheModelPOJO("interactions", 604800000L, 86400000L, 0L, "interaction_id", 1));
        databaseManager.insertMetaData(new CacheModelPOJO("interaction", 604800000L, 86400000L, 0L, "interaction_id", 1));
        databaseManager.insertMetaData(new CacheModelPOJO("post_queue_table", 604800000L, 86400000L, 0L, "interaction_id", 1));
        databaseManager.insertMetaData(new CacheModelPOJO("product", 600000L, 60000L, 0L, "product_id", 1));
        databaseManager.createTable("interaction", DatabaseConstants.CacheTableType.OBJECTS);
        databaseManager.createTable("post_queue_table", DatabaseConstants.CacheTableType.POST_QUEUE);
        databaseManager.createTable("interactions", DatabaseConstants.CacheTableType.OBJECTS);
        databaseManager.createTable("", DatabaseConstants.CacheTableType.MODEL);
    }

/*
    protected void setupLeakCanary() {
        enabledStrictMode();

        LeakCanary.install(this);
    }

    private static void enabledStrictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder() //
                .detectAll() //
                .penaltyLog() //
                .penaltyDeath() //
                .build());
    }*/

}
