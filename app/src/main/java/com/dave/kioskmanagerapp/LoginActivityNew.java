package com.dave.kioskmanagerapp;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.services.rekognition.model.Label;
import com.crashlytics.android.Crashlytics;
import com.dave.kioskmanagerapp.rekognition.LabelDetection;
import com.dave.kioskmanagerapp.services.ServiceCamera;
import com.dave.kioskmanagerapp.utils.AttributeNames;
import com.dave.kioskmanagerapp.utils.BroadcastNames;
import com.dave.kioskmanagerapp.utils.DetectedFacesModel;
import com.dave.kioskmanagerapp.utils.FacesMasterList;
import com.dave.kioskmanagerapp.utils.KioskManagerPerspective;
import com.dave.kioskmanagerapp.utils.MediaPlaying;
import com.dave.kioskmanagerapp.utils.SharedPreference;
import com.dave.kioskmanagerapp.utils.StickyImmersiveFullScreen;
import com.example.admin.daveai.broadcasting.MediaService;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.models.APIQueueTableRowModel;
import com.example.admin.daveai.database.models.SingletonTableRowModel;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.APIRoutes;
import com.example.admin.daveai.others.CacheModelPOJO;
import com.example.admin.daveai.others.ConnectDaveAI;
import com.example.admin.daveai.others.DaveAI;
import com.example.admin.daveai.others.DaveAIHelper;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.daveai.others.DaveSharedPreference;
import com.example.admin.daveai.others.PerspectiveHelper;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

public class LoginActivityNew extends Activity implements AttributeNames {

    private static final String TAG = "LoginActivityNew";
    TextView loginBtn,forceUpdate;
    DaveModels daveModels;
    EditText enterUserId, enterPassword;
    CheckBox cb;

    MyBroadCastReceiver myBroadCastReceiver;
    private static final int PERMISSION_REQUEST_CODE = 1;
    public final static int REQUEST_CODE = 11111;
    private long minimumTimeBetweenMotionDetection = 30000;
    private long lastTimeMotion = 0;
    int overlapThreshhold = 30;
    int whatToSayOnDetectionCount = 0;




    HashMap<String,MediaPlaying> currentlyPlayingMedia = new HashMap<>();

    private Handler mHandler = new Handler();

    String[] whatToSayOnDetectionList = new String[]{"Welcome!","Hey there we have some great brands in fashion","Hey there, you are looking great, why not checkout some of our hottest styles."};


    long AVAILABLE_PING_DELAY = 10*60*1000;
    private int FACE_COUNT_THRESHOLD = 5;
    private int FACE_MISSING_FRAME_THRESHOLD = 5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        android_id = "14fb48186a10bc42";//Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);
        DaveSharedPreference sp = new DaveSharedPreference(getApplicationContext());
        TextView androidId = (TextView)findViewById(R.id.android_id);
        androidId.setText(android_id);
        Log.e("FLOW--","onCreate()");
        StickyImmersiveFullScreen.getFullScreen(getWindow());
        if (Build.VERSION.SDK_INT >= 23)
        {
            if (checkPermission())
            {
                uiInit();
            } else {
                requestPermission(); // Code for permission
            }
        }
        else
        {
            uiInit();
        }

        daveModels = new DaveModels(LoginActivityNew.this, true);

    }




    private boolean checkPermission() {
        Log.e("FLOW--","checkPermission()");
        int result = ContextCompat.checkSelfPermission(LoginActivityNew.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read_res = ContextCompat.checkSelfPermission(LoginActivityNew.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int camera_res = ContextCompat.checkSelfPermission(LoginActivityNew.this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED&&read_res == PackageManager.PERMISSION_GRANTED&&camera_res == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        Log.e("FLOW--","requestPermission()");

        if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivityNew.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(LoginActivityNew.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(LoginActivityNew.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.e("FLOW--","onRequestPermissionsResult()");

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    uiInit();

                    Log.e("value", "Permission Granted, Now you can use local drive .");
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }
    private void uiInit(){
        Log.e("FLOW--","uiInit()");

        myBroadCastReceiver = new LoginActivityNew.MyBroadCastReceiver();
        registerMyReceiver();
        enterUserId = findViewById(R.id.enterUserId);
        enterPassword = findViewById(R.id.enterPassword);
        loginBtn = findViewById(R.id.loginBtn);
        forceUpdate = findViewById(R.id.forceUpdate);
        cb = findViewById(R.id.cb);

        if(!TextUtils.isEmpty(SharedPreference.getInstance(LoginActivityNew.this).getString("email_saved")) ) {
            enterUserId.setText(SharedPreference.getInstance(LoginActivityNew.this).getString("email_saved"));
            enterPassword.setText(SharedPreference.getInstance(LoginActivityNew.this).getString("pw_saved"));
        }else{
            enterUserId.setText("soham_store@i2ce_in");
            enterPassword.setText("123456");
        }

        Log.e(TAG,"Get User API KEy:-----------------  "+SharedPreference.getInstance(LoginActivityNew.this).getApiKey());



        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(new DaveSharedPreference(LoginActivityNew.this).readString(DaveSharedPreference.API_KEY))) {
                    loginToApp();
                }else{
                    getApkUpdateDetails();
                }

            }
        });
        if(getIntent().getBooleanExtra("autoLogin",false)){
            loginBtn.performClick();
        }

    }

    public static void requestSystemAlertPermission(Activity context, int requestCode) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return;
        final String packageName = context.getPackageName();
        final Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + packageName));

            context.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE){
            final Intent i = new Intent(LoginActivityNew.this, ServiceCamera.class);
            startService(i);
        }
    }

    private void startCamService(){
        if (Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(LoginActivityNew.this)) {
                Log.e("LOGGER","can draw overlay = "+canDrawOverlays(LoginActivityNew.this));
                /** if not construct intent to request permission */
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                /** request permission via start activity for result */
                startActivityForResult(intent, REQUEST_CODE);
            }else{
                final Intent i = new Intent(LoginActivityNew.this, ServiceCamera.class);
                startService(i);
            }
        }else{
            final Intent i = new Intent(LoginActivityNew.this, ServiceCamera.class);
            startService(i);
        }
    }
    public static boolean canDrawOverlays(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return true;
        else if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            return Settings.canDrawOverlays(context);
        } else {
            if (Settings.canDrawOverlays(context)) return true;
            try {
                WindowManager mgr = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                if (mgr == null) return false; //getSystemService might return null
                View viewToAdd = new View(context);
                WindowManager.LayoutParams params = new WindowManager.LayoutParams(0, 0, android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O ?
                        WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY : WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSPARENT);
                viewToAdd.setLayoutParams(params);
                mgr.addView(viewToAdd, params);
                mgr.removeView(viewToAdd);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    }
    private void sendMyBroadCast(String broadcastId)
    {
        try
        {
            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(broadcastId);

//            broadCastIntent.putExtra("data", video_id);
            Log.e("LOGGER","SENDING PLAYLIST BROADCAST "+broadcastId);
            sendBroadcast(broadCastIntent);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * This method is responsible to register an action to BroadCastReceiver
     * */
    private void registerMyReceiver() {

        try
        {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(MediaService.MEDIA_QUEUE_EMPTY);

            List broadcasts = Arrays.asList(BroadcastNames.values());
            for(int i = 0 ; i < broadcasts.size() ; i++){
                intentFilter.addAction(broadcasts.get(i).toString());
            }
            registerReceiver(myBroadCastReceiver, intentFilter);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }





    private void onComplete(ArrayList<String> labelsFound, String interactionId){
        try {
            for (int l = 0; l < detectedFaces.size(); l++) {
                if (detectedFaces.get(l).getAws_interaction_id().equals(interactionId)) {
                    //Patching media interaction with the labels detected
                    JSONObject patchBody = new JSONObject();
                    Gson gson = new Gson();
                    JSONArray jarr = new JSONArray(gson.toJson(labelsFound));
                    patchBody.put("customer_tags", jarr);
                    for (int a = 0; a < detectedFaces.get(l).getInteractions().size(); a++) {
                        detectedFaces.get(l).getInteractions().get(a).put("customer_tags", jarr);
                        new DaveModels(LoginActivityNew.this, true).patchObject("interaction", detectedFaces.get(l).getInteractions().get(a).getString("interaction_id"), patchBody, null);
                    }

                    detectedFaces.remove(detectedFaces.get(l));

                    l--;

                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    ArrayList<DetectedFacesModel> detectedFaces = new ArrayList<>();





    private void processNewFaceInFrame(org.opencv.core.Rect[] currentSet){
        if(detectedFaces!=null && detectedFaces.size()>0){

            for (int i = 0; i < currentSet.length; i++) {
                boolean overlapFound = false;
                for (int j = 0; j < detectedFaces.size(); j++) {
                    if (findOverlap(currentSet[i], detectedFaces.get(j).getFace()) > overlapThreshhold) {

                        Log.e("LOGGER","OVERLAP DETECTED");

                        overlapFound = true;

                        //increase count
                        detectedFaces.get(j).addCount();

                        //check for count greater than threshold
                        if(detectedFaces.get(j).getCount()>detectedFaces.get(j).getCountThreshold()){
                            Log.e("LOGGER","OVERLAP DETECTED count crossed thresh "+detectedFaces.get(j).getInteractions().size());

                            //TODO send all interactions
                            for(int x = 0 ; x < detectedFaces.get(j).getInteractions().size() ; x++){
                                try {
                                    DaveModels daveModels = new DaveModels(LoginActivityNew.this, true);
                                    daveModels.postObject("interaction", detectedFaces.get(j).getInteractions().get(x), new DaveAIListener() {
                                        @Override
                                        public void onReceivedResponse(JSONObject jsonObject) {

                                        }

                                        @Override
                                        public void onResponseFailure(int i, String s) {

                                        }
                                    });
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                if(TextUtils.isEmpty(detectedFaces.get(j).getAws_interaction_id())) {
                                    try {
                                        detectedFaces.get(j).setAws_interaction_id(detectedFaces.get(j).getInteractions().get(x).getString("interaction_id"));

                                        Toast.makeText(LoginActivityNew.this, "Face Detected, Fetching Customer Tags",Toast.LENGTH_SHORT).show();

                                        new LabelDetection().detectLabels(LoginActivityNew.this,detectedFaces.get(j).getImage(), detectedFaces.get(j).getAws_interaction_id(), new LabelDetection.LabelDetectionListener() {
                                            @Override
                                            public void onLabelDetected(List<Label> labels, String interactionId) {
                                                try {

                                                    ArrayList<String> labelsFound = new ArrayList<>();
                                                    for (Label label : labels) {

                                                        if(label.getConfidence()>50) {

                                                            labelsFound.add(label.getName().toLowerCase());
                                                        }
                                                    }

                                                    if(labels.size()>0){
                                                        LoginActivityNew.this.runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                Log.e(TAG,"labelsFound  = "+labelsFound.toString());
                                                                Toast.makeText(LoginActivityNew.this, "Detected Customer Tags - "+labelsFound.toString(),Toast.LENGTH_LONG).show();
                                                            }
                                                        });
                                                    }
                                                    for (int l = 0; l < detectedFaces.size(); l++) {
                                                        if (detectedFaces.get(l).getAws_interaction_id().equals(interactionId)) {

                                                            detectedFaces.get(l).setTags_detected(labels);


                                                            if(!detectedFaces.get(l).isCompleted()) {

                                                                HashMap<String, Object> cacheQueryData = new HashMap<>();


//                                                                cacheQueryData.put("media_tags", labelsForQuery);
                                                                HashMap<String, JSONObject> media_ids = new HashMap<>();
                                                                ArrayList<String> mediaIdsList = new ArrayList<>();
                                                                for (String key : masterMediaList.keySet()) {
                                                                    JSONArray mediaList = masterMediaList.get(key);
                                                                    for (int i = 0; i < mediaList.length(); i++) {
                                                                        media_ids.put(mediaList.getJSONObject(i).getString("media_id"), mediaList.getJSONObject(i));
                                                                        mediaIdsList.add(mediaList.getJSONObject(i).getString("media_id"));
                                                                    }
                                                                }
                                                                cacheQueryData.put("media_id", mediaIdsList);
                                                                LoginActivityNew.this.runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        try {
                                                                            new DaveModels(LoginActivityNew.this, true).getObjects("media", cacheQueryData, new DaveAIListener() {
                                                                                @Override
                                                                                public void onReceivedResponse(JSONObject jsonObject) {
                                                                                    try {
                                                                                        JSONArray data = jsonObject.getJSONArray("data");

                                                                                        JSONArray asdf = jsonObject.getJSONArray("data");
/*
                                                                                        for(int as = 0; as< asdf.length() ; as++ ){
                                                                                            Log.e("LOGGER","I GOT TAGS  = "+((JSONArray) asdf).getJSONObject(as).getJSONArray("media_tags"));
                                                                                        }
*/
                                                                                        for (String key : masterMediaList.keySet()) {

                                                                                            JSONArray alternatePlaylist = new JSONArray();
                                                                                            JSONArray mediaList = masterMediaList.get(key);
                                                                                            for (int i = 0; i < mediaList.length(); i++) {
                                                                                                for (int x = 0; x < data.length(); x++) {
                                                                                                    JSONObject mediaDta = data.getJSONObject(x);
                                                                                                    if (mediaDta.getString("media_id").equals(mediaList.getJSONObject(i).getString("media_id"))) {


                                                                                                        JSONArray mediaTags = mediaDta.getJSONArray("media_tags");
                                                                                                        for (int q = 0; q < mediaTags.length(); q++) {
                                                                                                            boolean foundMatch = false;
                                                                                                            for (int r = 0; r < labelsFound.size(); r++) {
                                                                                                                if (labelsFound.get(r).equals(mediaTags.getString(q))) {
                                                                                                                    alternatePlaylist.put(mediaList.getJSONObject(i));
                                                                                                                    foundMatch = true;
                                                                                                                    break;
                                                                                                                }
                                                                                                            }
                                                                                                            if(foundMatch){
                                                                                                                break;
                                                                                                            }
                                                                                                        }
                                                                                                        break;
                                                                                                    }

                                                                                                }
                                                                                            }

                                                                                            if (alternatePlaylist.length() > 0) {
                                                                                                LoginActivityNew.this.runOnUiThread(new Runnable() {
                                                                                                    @Override
                                                                                                    public void run() {
                                                                                                        Toast.makeText(LoginActivityNew.this, "Updated media specific to demographics", Toast.LENGTH_SHORT).show();
                                                                                                    }
                                                                                                });
                                                                                                Log.e("LOGGER", "WRITING DATA TO FILE for labels = " + labelsFound.toString());
                                                                                                writeToFile(key, alternatePlaylist.toString());
                                                                                                sendMyBroadCast(key);

                                                                                            }
                                                                                        }


                                                                                    } catch (Exception e) {
                                                                                        e.printStackTrace();
                                                                                    }
                                                                                }

                                                                                @Override
                                                                                public void onResponseFailure(int i, String s) {

                                                                                }
                                                                            });
                                                                        }catch (Exception e){
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                });

                                                            }else{
                                                                mHandler.post(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        onComplete(labelsFound, interactionId);
                                                                    }
                                                                });
                                                            }
                                                            break;
                                                        }
                                                    }
                                                }catch (Exception e){
                                                    e.printStackTrace();
                                                }

                                            }

                                            @Override
                                            public void onLabelNotDetected(String interactionId) {

                                            }
                                        });
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }

                    }else{
                        //increase the threshold
                        detectedFaces.get(j).addFrameSkipped();
                        if(detectedFaces.get(j).getFrameSkipped()>detectedFaces.get(j).getFrameSkippedThreshold()){
                            detectedFaces.get(j).setCompleted(true);
                            if(detectedFaces.get(j).getTags_detected().size()>0) {
                                ArrayList<String> labelsFound = new ArrayList<>();
                                for (Label label : detectedFaces.get(j).getTags_detected()) {
                                    if(label.getConfidence()>50) {
                                        labelsFound.add(label.getName().toLowerCase());
                                    }
                                }
                                onComplete(labelsFound,detectedFaces.get(j).getAws_interaction_id());
                            }
                        }
                    }
                }

                if(!overlapFound){
                    detectedFaces.add(new DetectedFacesModel(currentSet[i],FACE_COUNT_THRESHOLD,FACE_MISSING_FRAME_THRESHOLD,currentFaceFrameImage));
                }
            }
        }else{
            for (int i = 0; i < currentSet.length; i++) {
                detectedFaces.add(new DetectedFacesModel(currentSet[i],FACE_COUNT_THRESHOLD,FACE_MISSING_FRAME_THRESHOLD,currentFaceFrameImage));
            }
        }
    }



    private int findOverlap(org.opencv.core.Rect one,org.opencv.core.Rect two){

        int x11=0,y11=0,x12=0,y12=0,x21=0,y21=0,x22=0,y22=0;


        x11 = one.x;
        y11 = one.y;
        x12 = one.x+one.width;
        y12 = one.y+one.height;

        x21 = two.x;
        y21 = two.y;
        x22 = two.x+two.width;
        y22 = two.y+two.height;


        int left = Math.max(x11, x21);
        int right = Math.min(x12, x22);
        int bottom = Math.min(y12, y22);
        int top = Math.max(y11, y21);
        int areaOfIntersection = 0;
        areaOfIntersection = ((left-right)*(top-bottom));



        int areaOfSmallerRect = 0;

        if((one.height*one.width)<(two.height*two.width)){
            areaOfSmallerRect = (one.height*one.width);
        }else{
            areaOfSmallerRect = (two.height*two.width);
        }

        int percOfIntersection = ((areaOfIntersection)*100)/ areaOfSmallerRect;

        Log.e("LOGGER","findOverlap() percOfIntersection = "+percOfIntersection + "detected faces count = "+detectedFaces.size());

        return percOfIntersection;
    }





    TextToSpeech tts;
    String android_id;
    long currentHour = 0;

    byte[] currentFaceFrameImage = null;


    class MyBroadCastReceiver extends BroadcastReceiver
    {


        private void updatePlayCount(String media_data) {
            Log.e(TAG," updatePlayCount() ");
            try {
                JSONObject mediaData = new JSONObject(media_data);

                Calendar calender = new GregorianCalendar();

                calender.setTimeZone(TimeZone.getTimeZone("GMT"));
                calender.setTimeInMillis(System.currentTimeMillis());
                System.out.println(calender.get(Calendar.HOUR_OF_DAY) + ":" + calender.get(Calendar.MINUTE) + ":" + calender.getActualMinimum(Calendar.SECOND));

                calender.set(Calendar.MINUTE, 0);
                calender.set(Calendar.SECOND, 0);
                calender.set(Calendar.MILLISECOND, 0);
                System.out.println(calender.get(Calendar.HOUR_OF_DAY) + ":" + calender.get(Calendar.MINUTE) + ":" + calender.getActualMinimum(Calendar.SECOND));

                currentHour = calender.getTimeInMillis() / 1000;
//                    setupModelCacheMetaData("device_media_played","device_media_played_id");
                DaveModels daveModels = new DaveModels(LoginActivityNew.this, true);
                HashMap<String, Object> params = new HashMap<>();
                params.put("device_id", android_id);
                params.put("media_id", mediaData.getString("media_id"));
                params.put("played_time", currentHour);
                daveModels.getObjects("device_media_played", params, new DaveAIListener() {
                    @Override
                    public void onReceivedResponse(JSONObject jsonObject) {
                        try {
                            Log.e("LOGGER", "device played response = " + jsonObject);
                            if (jsonObject.getJSONArray("data").length() == 0) {
                                JSONObject vidPlayedJobj = new JSONObject();
                                vidPlayedJobj.put("device_id", android_id);
                                vidPlayedJobj.put("media_id", mediaData.getString("media_id"));
                                vidPlayedJobj.put("played_time", currentHour);
                                vidPlayedJobj.put("played_count", 1);
                                new DaveModels(LoginActivityNew.this, true).postObject("device_media_played", vidPlayedJobj, new DaveAIListener() {
                                    @Override
                                    public void onReceivedResponse(JSONObject jsonObject) {

                                    }

                                    @Override
                                    public void onResponseFailure(int i, String s) {

                                    }
                                });
                            } else {

                                JSONObject iupdateBody = new JSONObject();
                                iupdateBody.put("played_count", 1);
                                new DaveModels(LoginActivityNew.this, false).iupdateObject("device_media_played", jsonObject.getJSONArray("data").getJSONObject(0).getString("device_media_played_id"), iupdateBody, new DaveAIListener() {
                                    @Override
                                    public void onReceivedResponse(JSONObject jsonObject) {
                                        Log.e("LOGGER", jsonObject.toString());
                                    }

                                    @Override
                                    public void onResponseFailure(int i, String s) {

                                    }
                                });
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onResponseFailure(int i, String s) {

                    }
                }, false, false, false);

            }catch (Exception e){
                e.printStackTrace();
            }

        }
        @Override
        public void onReceive(Context context, Intent intent) {

            try {

                if (intent.getAction().equals(BroadcastNames.MEDIA_PLAYING.toString())) {
                    currentlyPlayingMedia.put(intent.getExtras().getString("app_suffix"),new MediaPlaying(intent.getExtras().getString("media_id"),intent.getExtras().getString("app_id"),intent.getExtras().getString("app_device_id")));

                    for(int i = 0 ; i < detectedFaces.size() ; i++){
                        try {
                            JSONObject interactionBody = new JSONObject();

                            interactionBody.put("interaction_type", "watched");
                            interactionBody.put("media_id", intent.getExtras().getString("media_id"));
                            interactionBody.put("app_id", intent.getExtras().getString("app_id"));
                            interactionBody.put("app_device_id", intent.getExtras().getString("app_device_id"));

                            interactionBody.put("interaction_id",new DaveModels(LoginActivityNew.this,true).createNewObjectId("interaction", interactionBody));



                            detectedFaces.get(i).getInteractions().add(interactionBody);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }


                }

                if (intent.getAction().equals(BroadcastNames.VIDEO_PLAYED.toString())) {

                    updatePlayCount(intent.getExtras().getString("media_id"));

                }
                if (intent.getAction().equals(BroadcastNames.IMAGE_PLAYED.toString())) {
                    Log.e("image_played","image played");
                    updatePlayCount(intent.getExtras().getString("media_id"));

                }else if(intent.getAction().equals(BroadcastNames.MOTION_DETECTED.toString())) {

                    Log.e("LOGGER", "BROADCAST RECEIVED = " + intent.getAction()+"   detectFaces size = "+detectedFaces.size());

                    for(int i = 0 ; i < detectedFaces.size() ; i++){
                        detectedFaces.get(i).addFrameSkipped();

                        if(detectedFaces.get(i).getFrameSkipped()>detectedFaces.get(i).getFrameSkippedThreshold()){
                            detectedFaces.get(i).setCompleted(true);
                            if(detectedFaces.get(i).getCount()>detectedFaces.get(i).getCountThreshold()) {
                                if (!TextUtils.isEmpty(detectedFaces.get(i).getAws_interaction_id()) && detectedFaces.get(i).getTags_detected().size() > 0) {
                                    //if detected face has crossed count thresh and has tags and interaction ids
                                    ArrayList<String> labelsFound = new ArrayList<>();
                                    for (Label label : detectedFaces.get(i).getTags_detected()) {
                                        if (label.getConfidence() > 50) {
                                            labelsFound.add(label.getName().toLowerCase());
                                        }
                                    }
                                    onComplete(labelsFound, detectedFaces.get(i).getAws_interaction_id());
                                }else{
                                    //if detected face has crossed count thresh and doesnt not have tags or interaction ids
                                    detectedFaces.remove(detectedFaces.get(i));
                                    i--;
                                }
                            }else{
                                //if detected face has not crossed count and has skipped too many frames
                                detectedFaces.remove(detectedFaces.get(i));
                                i--;
                            }
                        }
                    }


                    if(detectedFaces.size()==0) {

                        if ((System.currentTimeMillis() - lastTimeMotion) > minimumTimeBetweenMotionDetection) {

                            Toast.makeText(LoginActivityNew.this, "Person Detected, Played Audio",Toast.LENGTH_SHORT).show();
                            lastTimeMotion = System.currentTimeMillis();
                            Log.e(TAG, BroadcastNames.MOTION_DETECTED.name());
                            /*tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                                @Override
                                public void onInit(int status) {
                                    if (status != TextToSpeech.ERROR) {
                                        tts.setLanguage(Locale.UK);
                                        tts.speak(whatToSayOnDetectionList[whatToSayOnDetectionCount], TextToSpeech.QUEUE_FLUSH, null, "11");

                                        whatToSayOnDetectionCount = (whatToSayOnDetectionCount+1)%whatToSayOnDetectionList.length;
                                    }
                                }
                            });

                            tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                                @Override
                                public void onStart(String utteranceId) {

                                }

                                @Override
                                public void onDone(String utteranceId) {
                                    tts.stop();
                                    tts.shutdown();
                                }

                                @Override
                                public void onError(String utteranceId) {

                                }
                            });*/

                        }
                    }
                }else if(intent.getAction().equals(BroadcastNames.FACE_DETECTED.toString())) {

                    Log.e("LOGGER", "BROADCAST RECEIVED = " + intent.getAction()+"   detectFaces size = "+detectedFaces.size());
                    String faces = intent.getExtras().getString("faces");
                    currentFaceFrameImage = intent.getExtras().getByteArray("img");
                    org.opencv.core.Rect[] object = new Gson().fromJson(faces, org.opencv.core.Rect[].class);
                    if(object.length>0) {
                        processNewFaceInFrame(object);
                    }

                }else if(intent.getAction().equals("MEDIA_QUEUE_EMPTY")){
                    if(!checkInitMediaDone) {
                        checkInitMediaDone = true;
//                        checkAllDone();
                    }
                }



            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }



    private void loginToApp(){
        Log.e("FLOW--","loginToApp()");

        if (isEmpty(enterUserId)) {
            Toast.makeText(LoginActivityNew.this, "Please enter the UserId", Toast.LENGTH_SHORT).show();
        } else if (isEmpty(enterPassword)) {
            Toast.makeText(LoginActivityNew.this, "Please enter the Password", Toast.LENGTH_SHORT).show();
        } else {
            SharedPreference.getInstance(LoginActivityNew.this).saveString("email_saved",enterUserId.getText().toString());
            SharedPreference.getInstance(LoginActivityNew.this).saveString("pw_saved",enterPassword.getText().toString());
            ConnectDaveAI managerLogin = new ConnectDaveAI(LoginActivityNew.this);
            managerLogin.registerConnectDaveCallback(new ConnectDaveAI.OnConnectedWithDaveListener() {
                @Override
                public void onDaveConnected(boolean b) {
                    Log.e("LOGGER","onDaveConnected()");
                }
            });
            managerLogin.registerLoginCallback(new ConnectDaveAI.OnLoginSuccessListener() {
                @Override
                public void loginSucess(JSONObject userLoginDetails) {
                    storeManagerLoginResponseHandler(userLoginDetails);
                }

                @Override
                public void loginFailed(String s) {
                    Log.e("LOGGER","manager loginFailed()");
                }
            });

            try {
                JSONObject post_body = new JSONObject();
                post_body.put("user_id", enterUserId.getText().toString());
                post_body.put("password", enterPassword.getText().toString());
                post_body.put("redirect_to", "json");
                managerLogin.loginWithDaveAI(LoginActivityNew.this, post_body, false);

            }catch (Exception e){
                Toast.makeText(LoginActivityNew.this,"Error trying to Login please contact admin.",Toast.LENGTH_SHORT).show();
            }
        }

    }

    public static boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().equals("");
    }



    private void storeManagerLoginResponseHandler(final JSONObject userLoginDetails){
        Log.e("FLOW--","storeManagerLoginResponseHandler()");
        try {
            DaveModels deviceModel = new DaveModels(LoginActivityNew.this, true);
            deviceModel.getObject("store_manager", userLoginDetails.getString(USER_ID), new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject jsonObject) {
                    try {
                        Log.e("FLOW--", "storeManagerLoginResponseHandler().onTaskCompleted()");

                        JSONObject storeManagerData = jsonObject;
                        getDeviceData(storeManagerData.getString(STORE_ID), userLoginDetails.getString(USER_ID));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onResponseFailure(int i, String s) {
                    Log.e("LOGGER","get store manager failed == "+s);
                }
            },true,false,true);
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private String m_Text = "";

    private void getDeviceData(final String storeId, final String managerEmail){
        Log.e("FLOW--","getDeviceData()");

        /*test id  = "aa1bbe5006394fb9";*/
        android_id = "14fb48186a10bc42";//Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);

        final DaveModels deviceModelNew = new DaveModels(LoginActivityNew.this,true);
        try {
            deviceModelNew.getObject("device", android_id, new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject s) {
                    Log.e("FLOW--","getDeviceData().onTaskCompleted()");

                    try {
                        JSONObject jsonObject = s;
                        DatabaseManager databaseManager = DatabaseManager.getInstance(LoginActivityNew.this);
                        databaseManager.createTable("",DatabaseConstants.CacheTableType.SINGLETON);
                        databaseManager.insertMetaData(
                                new CacheModelPOJO("device", 86400 * 1000, 18000 * 1000, 0,"device_id",1)
                        );

                        databaseManager.insertOrUpdateSingleton("device",new SingletonTableRowModel("device",s.toString(),System.currentTimeMillis(),"POST"));


                        Log.e("LOGGER","device count = "+s+"  s.equals({})  =  "+s.toString().equals("{}") +"   s.length() = "+s.length());
                        if (s.equals("{}") || s.length()==0 ||((jsonObject.has("store_manager_email")&&jsonObject.has("store_id"))&&(jsonObject.getString("store_manager_email").equals(managerEmail)&&jsonObject.getString("store_id").equals(storeId)))) {
                            if(!jsonObject.has("device_name")||(jsonObject.has("device_name")&&jsonObject.getString("device_name").isEmpty())) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivityNew.this);
                                builder.setTitle("Enter Device Name");

                                // Set up the input
                                final EditText input = new EditText(LoginActivityNew.this);
                                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                                input.setInputType(InputType.TYPE_CLASS_TEXT);
                                builder.setView(input);
                                // Set up the buttons
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        m_Text = input.getText().toString();
                                        postDevice(m_Text, managerEmail, databaseManager, storeId,jsonObject);
                                    }
                                });
                                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });

                                builder.show();
                            }else{
                                postDevice(jsonObject.getString("device_name"), managerEmail, databaseManager, storeId,jsonObject);
                            }
                        } else {
                            loginDevice(android_id);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onResponseFailure(int i, String s) {

                }
            },true,true,true);

        }catch (Exception e){
            e.printStackTrace();
        }
    }



    private void postDevice(String device_name, String managerEmail,DatabaseManager databaseManager,String storeId, JSONObject jobj){
        try {
            DaveModels devicePost = new DaveModels(LoginActivityNew.this,true);
//            JSONObject jobj = new JSONObject();
            jobj.put("store_id", storeId);
            jobj.put("device_name", device_name);
            jobj.put("device_id", android_id);
            jobj.put("validated", true);
            jobj.put("store_manager_email", managerEmail);
            if(!jobj.has("device_app_data")) {
                jobj.put("device_app_data", new HashMap<String, String>());
            }
            jobj.put("password","123456");
            databaseManager = DatabaseManager.getInstance(LoginActivityNew.this);
            setupModelCacheMetaData("device","device_id");
            databaseManager.createTable("",DatabaseConstants.CacheTableType.SINGLETON);

            databaseManager.insertOrUpdateSingleton("device",new SingletonTableRowModel("device",jobj.toString(),System.currentTimeMillis(),"POST"));


            devicePost.postObject("device", jobj, true, new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject jsonObject) {
                    Log.e("API_RESPONSE", jsonObject.toString());
                    loginDevice(android_id);
                }

                @Override
                public void onResponseFailure(int i, String s) {
//                                    loginAttemptFailed();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            loginAttemptFailed();
        }
    }




    private void loginAttemptFailed(){
        loginAttemptFailed("Login Failed. Please Try Again");
    }

    private void loginAttemptFailed(String s){
        Log.e("FLOW--","loginAttemptFailed()");

        SharedPreference.getInstance(LoginActivityNew.this).clearAllCache(LoginActivityNew.this);
        Toast.makeText(LoginActivityNew.this,"Login Failed. Please Try Again",Toast.LENGTH_LONG).show();

    }
    String[] loadModelList = {"device","app_device","device_media_played","media","interaction","customer"};
    String[] loadModelIdList = {"device_id","app_device_id","device_media_played_id","media_id","interaction_id","customer_id"};
    private void setupModelCacheMetaData(String modelName,String modelIdName){

        Log.e(TAG,"setupModelCacheMetaData()");

        DatabaseManager databaseManager = DatabaseManager.getInstance(LoginActivityNew.this);
        databaseManager.createTable(modelName,DatabaseConstants.CacheTableType.OBJECTS);
        databaseManager.insertMetaData(
                new CacheModelPOJO(modelName, 86400 * 1000, 18000 * 1000, 0,modelIdName,1)
        );
    }


    int versionCode = 0;
    int latestVersion=0;
    String appURI = "";
    String appName = "MyApp";
    public void getApkUpdateDetails() {
        Log.e("FLOW--","getApkUpdateDetails()");

        versionCode = BuildConfig.VERSION_CODE;
        com.example.admin.daveai.others.DaveSharedPreference sp = new com.example.admin.daveai.others.DaveSharedPreference(getApplicationContext());
        Log.e("LOGGER","API KEY = "+sp.readString(com.example.admin.daveai.others.DaveSharedPreference.API_KEY));
        Log.e("LOGGER","USER ID = "+sp.readString(com.example.admin.daveai.others.DaveSharedPreference.USER_ID));
        String userId = sp.readString(com.example.admin.daveai.others.DaveSharedPreference.USER_ID);
        String apiKey = sp.readString(com.example.admin.daveai.others.DaveSharedPreference.API_KEY);
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                Log.e("FLOW--","getApkUpdateDetails().onTaskCompleted()");
                boolean noNeedForUpdate = true;
                if(response == null || response.isEmpty()){
                    //TODO Enter Onboarding Activity
//                intentToOnBoardingActivity();
                }
                else {
                    try {
                        JSONObject data = new JSONObject(response);
                        Iterator<String> keys = data.keys();
                        while (keys.hasNext()) {
                            String key = keys.next();
                            if (data.get(key) instanceof JSONArray) {
                                JSONArray data_array = data.getJSONArray(key);
                                for (int i = 0; i < data_array.length(); i++) {
                                    JSONObject getData = data_array.getJSONObject(i);
                                    if (getData.has("version_code") && !getData.isNull("version_code"))
                                        latestVersion = getData.getInt("version_code");
                                    if (getData.has("app_url") && !getData.isNull("app_url"))
                                        appURI = getData.getString("app_url");
                                    if (getData.has("app_name") && !getData.isNull("app_name"))
                                        appName = getData.getString("app_name");
                                    if(getData.has("app_type")&&getData.getString("app_type").equals("supervisor_app")){
                                        noNeedForUpdate = false;
                                    }

                                }
                            }
                        }
                        Log.e(TAG,"Current App  Version :----"+versionCode+"  ************Latest ServerApp Version:------------"+latestVersion);
                        Log.e(TAG,"No Need for update :----"+noNeedForUpdate);
                        if(latestVersion > versionCode&&!noNeedForUpdate){
                            dialogToAskUpdateApp();
                        }else {
                            //TODO Enter Onboarding Activity
                            callModelsInOrder();

                        }

                    } catch (Exception e) {
                        Log.e("Exception:- ", "App Update Response  Error == " + e.getMessage());

                    }
                }
            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {
                Log.e("FLOW--","getApkUpdateDetails().onTaskFailure()");

                Log.e(TAG, "App Update onTaskFailure  Error  (RequestCode)== " +requestCode);
                if(requestCode == 401 || requestCode == 402 || requestCode == 403){
                    loginToApp();
                }

            }
        };
        Model model1 = new Model(LoginActivityNew.this);
        model1.getAppConfigurationDetails( userId,apiKey,postTaskListener);

    }


    private BroadcastReceiver downloadReceiver;
    private ProgressDialog pDialog;
    private DownloadManager downloadManager;
    private void dialogToAskUpdateApp(){
        Log.e("FLOW--","dialogToAskUpdateApp()");

        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivityNew.this);
        builder
                .setMessage("Upgrade is available for this application")
                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    //if the user agrees to upgrade
                    public void onClick(DialogInterface dialog, int id) {

                        pDialog = new ProgressDialog(LoginActivityNew.this);
                        pDialog.setMessage("Please wait application is updating...");
                        pDialog.setCancelable(false);
                        pDialog.show();
                        checkUpdate();
                        //start downloading the file using the download manager
                        downloadManager = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
                        Uri Download_Uri = Uri.parse(appURI);
                        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
                        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                        request.setAllowedOverRoaming(false);
                        request.setTitle(appName);
                        request.setDescription("Downloading " + appName + ".apk");
                        request.setVisibleInDownloadsUi(true);
                        request.setMimeType("application/vnd.android.package-archive");
                        request.setDestinationInExternalFilesDir(LoginActivityNew.this, Environment.DIRECTORY_DOWNLOADS,"MyAndroidApp.apk");
                        downloadReference = downloadManager.enqueue(request);
                        // Log.e("downloadReference ","downloadReference:----"+downloadReference);
                    }
                })
                .setNegativeButton("Remind Later", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.cancel();
//                        loginToApp();

                    }
                });
        //show the alert message
        builder.create().show();

    }

    private long downloadReference;
    private void checkUpdate() {
        Log.e("FLOW--","checkUpdate()");

        downloadReceiver =new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e("FLOW--","downloadReceiver.onReceive()");

                //check if the broadcast message is for our Enqueued download
                String action = intent.getAction();
                assert action != null;
                if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
                    long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                    Log.e("GetBroadcast:----  ", "referenceId:----  " + referenceId);
                    if (downloadReference == referenceId) {
                        Uri uri = downloadManager.getUriForDownloadedFile(downloadReference);
                        Log.e("Uri uri:----  ", "Uri uri before :----  " + uri);
                        String filePath = "";
                        if ("content".equals(uri.getScheme())) {
                            Cursor cursor = getContentResolver().query(uri, new String[]
                                    {android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                            assert cursor != null;
                            cursor.moveToFirst();
                            filePath = cursor.getString(0);
                            cursor.close();
                            uri = Uri.fromFile(new File(filePath));
                        }
                        Log.e("Uri Path :----  ", "uri Path  after ----  " + uri);
                        try {
                            SharedPreference.getInstance().saveString("unzipped","");
                            //start the installation of the latest version
                            Intent downloadIntent;
                            // Start the standard installation
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                Log.e("filePath:----  ", "filePath----  " + filePath);
                                Uri fileUri = FileProvider.getUriForFile(getBaseContext(), BuildConfig.APPLICATION_ID + ".provider", new File(filePath));
                                downloadIntent = new Intent(Intent.ACTION_VIEW, fileUri);
                                downloadIntent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
                                downloadIntent.setDataAndType(fileUri, "application/vnd.android.package-archive");
                                downloadIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                downloadIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);


                            } else {
                                Log.e("intent2:---- ", " Before Intent uri----" + uri);
                                downloadIntent = new Intent(Intent.ACTION_VIEW);
                                downloadIntent.setDataAndType(uri, "application/vnd.android.package-archive");
                                downloadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                            }
                            if(pDialog.isShowing()){
                                pDialog.dismiss();
                            }
                            startActivity(downloadIntent);
                            LoginActivityNew.this.finish();
                        } catch (Exception e) {
                            Log.e("Update App", "Error during updating App :---------- " + e.getMessage());
                        }


                    }
                }

            }
        };

        //Broadcast receiver for the download manager
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(downloadReceiver, filter);
    }





    private void loginDevice(String s){
        Log.e("LOGGER","android id sent for login = "+s);
        Log.e(TAG,"loginDevice()");
        ShowProgressDialog();
        ConnectDaveAI connectDaveAI = new ConnectDaveAI(LoginActivityNew.this);
        connectDaveAI.registerConnectDaveCallback(new ConnectDaveAI.OnConnectedWithDaveListener() {
            @Override
            public void onDaveConnected(boolean b) {
                Log.e(TAG,"loginDevice().onDaveConnected()");
                HideProgressDialog();
                /*for(int i = 0 ; i < loadModelList.length ; i ++){
                    setupModelCacheMetaData(loadModelList[i],loadModelIdList[i]);
                    Log.e("LOGGER","called getmodel for "+loadModelList[i]);
                    getModel(loadModelList[i]);
                }

                getApkUpdateDetails();*/
//                callObjectsInOrder();
            }
        });
        connectDaveAI.registerLoginCallback(new ConnectDaveAI.OnLoginSuccessListener() {
            @Override
            public void loginSucess(JSONObject userLoginDetails) {

                Log.e("FLOW--", "loginDevice().loginSuccess()");
                HideProgressDialog();
                try {
                    Log.e("FLOW--", "loginDevice().loginSuccess()");

                    Log.e("ASDASD", "DEVICE LOGGED IN " + userLoginDetails.getString(API_KEY));

                    HideProgressDialog();
                    for(int i = 0 ; i < loadModelList.length ; i ++){
                        setupModelCacheMetaData(loadModelList[i],loadModelIdList[i]);
                        Log.e("LOGGER","called getmodel for "+loadModelList[i]);
                        getModel(loadModelList[i]);
                    }

                    getApkUpdateDetails();
                    new PerspectiveHelper().getPerspective(LoginActivityNew.this, KioskManagerPerspective.perspectiveName, new DaveAIListener() {
                        @Override
                        public void onReceivedResponse(JSONObject jsonObject) {
                            KioskManagerPerspective.getInstance().savePerspectivePreferenceValue(LoginActivityNew.this);
                        }

                        @Override
                        public void onResponseFailure(int i, String s) {

                        }
                    });

                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(LoginActivityNew.this,e.getMessage(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void loginFailed(String s) {
                loginAttemptFailed();
                HideProgressDialog();
                Log.e("FLOW--","loginDevice().loginFailed()");

            }
        });
        try {
            JSONObject post_body = new JSONObject();
            post_body.put("user_id", s);
            post_body.put("password", "123456");
            post_body.put("redirect_to", "json");
            connectDaveAI.login(LoginActivityNew.this, post_body, false);
        }catch (Exception e){
            e.printStackTrace();
        }
//        connectDaveAI.loginWithDaveAI(s,"123456",false);
    }


    @Override
    protected void onDestroy() {
        Log.e("FLOW--","onDestroy()");

        super.onDestroy();

        unregisterReceiver(myBroadCastReceiver);
    }

    AlertDialog b;
    AlertDialog.Builder dialogBuilder;

    public void ShowProgressDialog() {
        Log.e("FLOW--","showProgressDialog()");
        dialogBuilder = new AlertDialog.Builder(LoginActivityNew.this);
        LayoutInflater inflater = (LayoutInflater) getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View dialogView = inflater.inflate(R.layout.progress_dialog_layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        b = dialogBuilder.create();
        b.show();
    }

    public void HideProgressDialog(){
        Log.e("FLOW--","HideProgressDialog()");

        if(b.isShowing()) {
            b.dismiss();
        }
    }


    private void getModel(String modelName){
        try {
            DaveModels daveModels = new DaveModels(LoginActivityNew.this, true);
            daveModels.getModel(modelName, new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject jsonObject) {
                    Log.e(TAG,"GET MODEL COMPLETE = "+jsonObject);

                }

                @Override
                public void onResponseFailure(int i, String s) {

                }
            } );
        }catch ( Exception e){
            e.printStackTrace();
        }
    }


    private void callModelsInOrder(){
        try {

            new DaveModels(LoginActivityNew.this, true).getModel("media", new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject jsonObjectProduct) {
                    try {
                        new ModelUtil(LoginActivityNew.this).saveIDAttrNameOfCoreModel("product", jsonObjectProduct.toString());
                        new DaveModels(LoginActivityNew.this, true).getModel("customer", new DaveAIListener() {
                            @Override
                            public void onReceivedResponse(JSONObject jsonObjectCustomer) {
                                Log.e("LOGGER","customer model = "+jsonObjectCustomer);
                                new ModelUtil(LoginActivityNew.this).saveIDAttrNameOfCoreModel("customer", jsonObjectCustomer.toString());
                                try {
                                    new DaveModels(LoginActivityNew.this, true).getModel("interaction", new DaveAIListener() {
                                        @Override
                                        public void onReceivedResponse(JSONObject jsonObjectInteraction) {
                                            ModelUtil modelUtil = new ModelUtil(LoginActivityNew.this);
                                            modelUtil.saveIDAttrNameOfCoreModel("interaction", jsonObjectInteraction.toString());
                                            modelUtil.saveInteractionModelAttributes("interaction");
                                            new APICallAsyncTask(new APIResponse() {
                                                @Override
                                                public void onTaskCompleted(String s) {
                                                    setupModelCacheMetaData("interaction_stages_table","interaction_stages_table");
                                                    new ConnectDaveAI(LoginActivityNew.this).saveInteractionStagesDetails(s);
                                                    callObjectsInOrder();
                                                }

                                                @Override
                                                public void onTaskFailure(int i, String s) {
                                                    loginAttemptFailed(s);
                                                }
                                            }, LoginActivityNew.this, "GET", false).execute(APIRoutes.interactionStagesAPI());

                                        }

                                        @Override
                                        public void onResponseFailure(int i, String s) {
                                            loginAttemptFailed(s);
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    loginAttemptFailed(e.getMessage());
                                }
                            }

                            @Override
                            public void onResponseFailure(int i, String s) {

                                loginAttemptFailed(s);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        loginAttemptFailed(e.getMessage());
                    }
                }

                @Override
                public void onResponseFailure(int i, String s) {

                    loginAttemptFailed(s);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
            loginAttemptFailed(e.getMessage());
        }
    }

    private void callObjectsInOrder(){
        ShowProgressDialog();
        HashMap<String,Object> params = new HashMap<>();
        params.put("device_id",android_id);
        DaveSharedPreference sharedPreference = new DaveSharedPreference(LoginActivityNew.this);
        sharedPreference.writeString("_interaction_id_attr_name", "interaction_id");
        DatabaseManager.getInstance(LoginActivityNew.this).insertOrUpdateCustomSingleton( "_interaction_id_attr_name",
                new SingletonTableRowModel("_interaction_id_attr_name","interaction_id",
                        System.currentTimeMillis(),"_interaction_id_attr_name"));
        try {
            DaveModels daveModels = new DaveModels(LoginActivityNew.this, true);
            daveModels.getObjects(loadModelList[0], params, new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject jsonObject) {
                    checkDeviceAppMediaDone = true;

                    try {
                        DaveModels daveModels2 = new DaveModels(LoginActivityNew.this, true);
                        daveModels2.getObjects(loadModelList[1], params, new DaveAIListener() {
                            @Override
                            public void onReceivedResponse(JSONObject jsonObject) {
                                checkDeviceDone = true;

                                try {
                                    new DaveModels(LoginActivityNew.this, true).getObjects("media", new HashMap<>(), new DaveAIListener() {
                                        @Override
                                        public void onReceivedResponse(JSONObject jsonObject) {
                                            new MediaService().startBackgroundService(LoginActivityNew.this);
//                                            checkAllDone();
                                            ShowProgressDialog();
                                            new DatabaseManager().alertOnMediaQueueIsEmpty(new DatabaseManager.OnAPIQueueEmptyListener() {
                                                @Override
                                                public void isEmpty() {
                                                    LoginActivityNew.this.runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            HideProgressDialog();
                                                            checkMediaDone = true;
                                                            checkAllDone();
                                                        }
                                                    });

                                                }
                                            },(1000));

                                        }

                                        @Override
                                        public void onResponseFailure(int i, String s) {

                                        }
                                    });
                                } catch (Exception e){
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onResponseFailure(int i, String s) {

                            }
                        },true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onResponseFailure(int i, String s) {

                }
            },true);
        }catch (Exception e){
            e.printStackTrace();
        }


    }



    private void writeToFile(String filename, String data){
        try {
            Log.e("LOGGER","WRITING DATA TO FILE = "+filename);
            Log.e("LOGGER","WRITING DATA = "+data);
            Writer output = null;
            File file = new File(Environment.getExternalStorageDirectory().getPath() + "/dave/cached_files/" + filename + ".json");
            if(file.exists()){
                file.delete();
            }
            output = new BufferedWriter(new FileWriter(file));
            output.write(data);
            output.close();
        } catch (Exception e) {
            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
    boolean checkDeviceDone = false;
    boolean checkDeviceAppMediaDone = false;
    boolean checkMediaDone = false;
    boolean checkInitMediaDone = false;

    HashMap<String,JSONArray> masterMediaList = new HashMap<>();
    ArrayList<JSONObject> videos = new ArrayList<>();
    ArrayList<JSONObject> images = new ArrayList<>();
    private void checkAllDone(){
        if(DatabaseManager.getInstance(LoginActivityNew.this).getDataFromTable(DatabaseConstants.MEDIA_QUEUE_TABLE_NAME).getCount() == 0)
            checkMediaDone = true;
        if(checkDeviceAppMediaDone&&checkDeviceDone&&checkMediaDone){
            try {

                HashMap<String,Object> params = new HashMap<>();
                params.put("device_id",android_id);
                DaveModels daveModels = new DaveModels(LoginActivityNew.this, true);
                daveModels.getObjects("app_device", params, new DaveAIListener() {
                    @Override
                    public void onReceivedResponse(JSONObject jsonObject) {
                        try {
                            JSONArray jarData = jsonObject.getJSONArray("data");
                            if(jarData.length()==0){
                                Intent i = new Intent(LoginActivityNew.this,MainActivity.class);
                                i.putExtra("sendHeaders",true);
                                i.putExtra("url",new DaveSharedPreference(LoginActivityNew.this).readString(DaveSharedPreference.BASE_URL)+"/go_to/cms?device_id="+android_id);
                                startActivity(i);
                            }else {
                                for (int i = 0; i < jarData.length(); i++) {
                                    JSONArray mediaListForFile = new JSONArray();
                                    JSONObject jobjMediaList = jarData.getJSONObject(i);
                                    JSONArray jarMediaList = jobjMediaList.getJSONArray("media_ids");
                                    for (int j = 0; j < jarMediaList.length(); j++) {
                                        JSONObject mediaMap = new JSONObject();
                                        mediaMap.put("media_id", jarMediaList.getString(j));
                                        mediaMap.put("media_url", jobjMediaList.getJSONArray("media_urls").getString(j));
                                        mediaListForFile.put(mediaMap);
                                    }
                                    JSONObject mediaData = new JSONObject();
                                    mediaData.put("media", mediaListForFile);
                                    mediaData.put("settings", jobjMediaList.getJSONObject("settings"));
                                    writeToFile(jobjMediaList.getString("app_suffix"), mediaData.toString());
                                    masterMediaList.put(jobjMediaList.getString("app_suffix"), mediaListForFile);

                                }
                                DaveModels deviceDaveModel = new DaveModels(LoginActivityNew.this, true);
                                deviceDaveModel.getObjects("device", params, new DaveAIListener() {
                                    @Override
                                    public void onReceivedResponse(JSONObject jsonObject) {
                                        try {
                                            startAvailablePing();
                                            final DaveModels deviceModelNew = new DaveModels(LoginActivityNew.this, true);
                                            try {
                                                deviceModelNew.getObject("device", android_id, new DaveAIListener() {
                                                    @Override
                                                    public void onReceivedResponse(JSONObject s) {
                                                        try {
                                                            Log.e("FLOW--", "afterlogin getdevice().onTaskCompleted()");
                                                            Crashlytics.setUserIdentifier(s.toString());
                                                            Crashlytics.setUserName(android_id);
                                                            Intent i = new Intent(LoginActivityNew.this, AppActivity.class);
                                                            i.putExtra("app_data", jsonObject.getJSONArray("data").getJSONObject(0).getJSONObject("device_app_data").toString());
                                                            LoginActivityNew.this.startActivity(i);
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                            loginAttemptFailed("Login Failed. Please Try Again " + e.getMessage());
                                                        }
                                                    }

                                                    @Override
                                                    public void onResponseFailure(int i, String s) {

                                                    }
                                                }, false, false, true);

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onResponseFailure(int i, String s) {

                                    }
                                });
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onResponseFailure(int i, String s) {

                    }
                },false,false,false);

            }catch (Exception e){
                e.printStackTrace();
            }
        }else{

            startBackgroundMediaCacheService();
        }

        HideProgressDialog();
    }


    int postedCount = 0;

    private void postAppDevice(ArrayList<String> appList){
        try {
            int appHeight = 0;
            switch (appList.size()) {
                case 1:
                case 2:
                    appHeight = 100 / appList.size();
                    break;
            }

            if (appHeight == 0) {

            } else {
                for(int i = 0 ; i < appList.size() ; i++) {

                    JSONObject postBody = new JSONObject();
                    postBody.put("app_id", appList.get(i));
                    postBody.put("x1", 0);
                    postBody.put("y1", appHeight*i);
                    postBody.put("x2", 100);
                    postBody.put("y2", appHeight*(i+1));
                    postBody.put("device_id",android_id);
                    postBody.put("app_position",(i+1));
                    new DaveModels(LoginActivityNew.this, true).postObject("app_device", postBody, new DaveAIListener() {
                        @Override
                        public void onReceivedResponse(JSONObject jsonObject) {
                            postedCount+=1;
                            if(postedCount == appList.size()){
                                checkAllDone();
                            }
                        }

                        @Override
                        public void onResponseFailure(int i, String s) {

                        }
                    });
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }





    private  void startBackgroundMediaCacheService(){
        //Start service to post data
        Log.e("LOGGER","starting media cache service");

        Intent msgIntent = new Intent(LoginActivityNew.this,MediaService.class);
        LoginActivityNew.this.startService(msgIntent);
    }


    private void startAvailablePing(){
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                postAvailable();

                handler.postDelayed(this, AVAILABLE_PING_DELAY);
            }
        };

        handler.postDelayed(runnable, AVAILABLE_PING_DELAY);
    }

    private void postAvailable(){
        try {
            DaveModels daveModels = new DaveModels(LoginActivityNew.this, true);
            JSONObject uptimeObj = new JSONObject();
            uptimeObj.put("device_status","available");
            uptimeObj.put("check_time",System.currentTimeMillis()/1000);
            daveModels.postObject("device_uptime", uptimeObj, false, new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject jsonObject) {

                }

                @Override
                public void onResponseFailure(int i, String s) {
                    Log.e("LOGGER","error code = "+i);
                    if(i == 408|| i == -1){
                        //timeout
                        try {
                            JSONObject downtimeObj = new JSONObject();
                            downtimeObj.put("device_status", "unavailable");
                            downtimeObj.put("check_time", System.currentTimeMillis() / 1000);
                            DatabaseManager.getInstance(LoginActivityNew.this).insertAPIQueueData(
                                    new APIQueueTableRowModel(
                                            APIRoutes.postObjectAPI("device_uptime"), DaveConstants.APIRequestMethod.POST.name(), downtimeObj.toString(), "",
                                            "", DatabaseConstants.APIQueueStatus.PENDING.name(), 1, DatabaseConstants.CacheTableType.POST_QUEUE.getValue()
                                    )
                            );
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
