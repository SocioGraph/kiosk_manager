package com.dave.kioskmanagerapp;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.dave.kioskmanagerapp.utils.StickyImmersiveFullScreen;
import com.example.admin.daveai.others.DaveSharedPreference;

import java.util.HashMap;


public class MainActivity extends AppCompatActivity {


    WebView browser;
    long shutOffTime = 0;
    boolean wasTouched = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_fragment);

        StickyImmersiveFullScreen.getFullScreen(getWindow());

        browser = findViewById(R.id.webView);
        if(getIntent().getExtras().containsKey("url")){
            Log.e("LOGGER","GOT URL OPEN REQUEST = "+getIntent().getExtras().getString("url"));

            // Enable javascript
            browser.getSettings().setJavaScriptEnabled(true);
            browser.getSettings().setAllowFileAccess(true);
            browser.getSettings().setAllowContentAccess(true);
            browser.getSettings().setAllowFileAccessFromFileURLs(true);
            browser.getSettings().setAllowUniversalAccessFromFileURLs(true);
            // Set WebView client
            browser.setWebChromeClient(new WebChromeClient());

            browser.setWebViewClient(new WebViewClient() {

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }
            });
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                CookieManager.getInstance().setAcceptThirdPartyCookies(browser, true);
            } else {
                CookieManager.getInstance().setAcceptCookie(true);
            }
            boolean sendHeaders = false;
            if(getIntent().getExtras().containsKey("sendHeaders")){
                sendHeaders = getIntent().getExtras().getBoolean("sendHeaders");
            }

            if(sendHeaders){
                HashMap<String,String> headers = new HashMap<>();

                headers.put("X-I2CE-API-KEY",new DaveSharedPreference(MainActivity.this).readString(DaveSharedPreference.API_KEY));
                headers.put("X-I2CE-USER-ID",new DaveSharedPreference(MainActivity.this).readString(DaveSharedPreference.USER_ID));
                headers.put("X-I2CE-ENTERPRISE-ID",new DaveSharedPreference(MainActivity.this).readString(DaveSharedPreference.EnterpriseId));
                headers.put("Access-Control-Allow-Origin","*");

                browser.loadUrl(getIntent().getExtras().getString("url"),headers);

            }else {
                // Load the webpage
                browser.loadUrl(getIntent().getExtras().getString("url"));
            }
            if(getIntent().getExtras().containsKey("shutOffTime")) {
                shutOffTime = getIntent().getExtras().getInt("shutOffTime")*1000;
            }
            if(shutOffTime!=0) {
                startTimer(shutOffTime);

                GestureDetectorCompat tapGestureDetector = new GestureDetectorCompat(this, new TapGestureListener());

                browser.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        tapGestureDetector.onTouchEvent(event);
                        return false;
                    }
                });
            }
        }

    }

    private void startTimer(long shutOffTime){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                if(wasTouched){
                    wasTouched = false;
                    startTimer(shutOffTime);
                }else{
                    finish();
                }

            }
        }, shutOffTime);
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    class TapGestureListener extends GestureDetector.SimpleOnGestureListener{

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            // Your Code here
            Log.e("ImageFragment", "clicked webview");
            wasTouched = true;
            return false;
        }
    }
}

