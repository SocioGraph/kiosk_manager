package com.dave.kioskmanagerapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ServiceStarter extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("LOGGER","AUTO LOGGER BROADCAST!!!");
        Intent i = new Intent(context, LoginActivityNew.class);
        i.putExtra("autoLogin",true);
        context.startActivity(i);
    }
}