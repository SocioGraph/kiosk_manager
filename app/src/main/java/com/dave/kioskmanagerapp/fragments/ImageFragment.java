package com.dave.kioskmanagerapp.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dave.kioskmanagerapp.MainActivity;
import com.dave.kioskmanagerapp.R;
import com.dave.kioskmanagerapp.adapter.ImageAdapter;
import com.dave.kioskmanagerapp.utils.BroadcastNames;
import com.dave.kioskmanagerapp.utils.FragmentUtils;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveModels;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Timer;
import java.util.TimerTask;

public class ImageFragment extends ParentFragment {

    JSONArray images = new JSONArray();
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static final int PERMISSION_REQUEST_CODE = 12;
    View rootView;
 /*   @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

    }*/

    @Override
    public View provideYourFragmentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.image_fragment, container, false);
        Log.e("LOGGER","ImageViewer Opened");
        uiInit(rootView);

        app_suffix = getArguments().getString("app_suffix");
        app_id = getArguments().getString("app_id");
        app_device_id = getArguments().getString("app_device_id");

        if(getArguments().containsKey("settings")){
            try {
                settingsString = getArguments().getString("settings");
                settingsJSON = new JSONObject(settingsString);
            }catch (Exception e){
                e.printStackTrace();
            }
        }


        trackOnViewClicked(mPager,settingsJSON);

        /*mPager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ImageFragment", "clicked fragment");
                if(settingsJSON.length()>0){
                    if(settingsJSON.has("onclick")){
                        if(settingsJSON.has("url")){
                            new FragmentUtils().openApp(getActivity(),new MainActivity(),"http://www.google.com");
                        }
                    }
                }
            }
        });*/


        return rootView;
    }

    @Override
    public boolean onMediaClicked() {

        try {
            Log.e("LOGGER", "clicked on media id = "+
                    images.getJSONObject(currentPage).getString("media_id"));
            DaveModels daveModels = new DaveModels(getActivity(),true);
            JSONObject mediajson = daveModels.getObject("media", images.getJSONObject(currentPage).getString("media_id"));
            JSONObject jobj = new JSONObject();
            if(mediajson.has("settings")) {
                jobj = mediajson.getJSONObject("settings");
            }
            Log.e("LOGGER","MEDIA DATA!!!!!!!"+jobj);
            if (jobj.length() > 0) {
                if (jobj.has("onClick") && jobj.getString("onClick").equals("webview")) {
                    if (jobj.has("url")) {
                        if (jobj.has("shutOffTime")) {
                            new FragmentUtils().openApp(getActivity(), new MainActivity(), jobj.getString("url"), jobj.getInt("shutOffTime"));
                        }
                    }
                } else if (jobj.has("onClick") && jobj.getString("onClick").equals("app")) {
                    if (jobj.has("app_id")) {
                        if (jobj.has("shutOffTime")) {
//                                new FragmentUtils().openOtherApp(getActivity(),"com.visualiser.dave.sociograph.davevisualiser",30);
                            new FragmentUtils().openOtherApp(getActivity(), jobj.getString("app_id"), jobj.getInt("shutOffTime"));
                        }
                    }
                } else if (jobj.has("onClick") && jobj.getString("onClick").equals("map")) {
                    //TODO to be implemented
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return false;
    }

    private String app_suffix = "";
    private String app_device_id = "";
    private String app_id = "";
    private String settingsString = "";
    private JSONObject settingsJSON = new JSONObject();


    public void setApp_suffix(String app_suffix) {
        this.app_suffix = app_suffix;
    }

    private void uiInit(View view){
        try {
            registerMyReceiver();
            File yourFile = new File(Environment.getExternalStorageDirectory(), "/dave/cached_files/"+app_suffix+".json");
            Log.e("LOGGER","OPENING FILE = "+Environment.getExternalStorageDirectory()+ "/dave/cached_files/"+app_suffix+".json");
            FileInputStream stream = new FileInputStream(yourFile);
            String jsonStr = null;
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

                jsonStr = Charset.defaultCharset().decode(bb).toString();
                JSONObject jobj = new JSONObject(jsonStr);
                images =jobj.getJSONArray("media");
                Log.e("LOGGER","IMAGE DATA = "+images);
                if(images.length()>0){

                    mPager = (ViewPager) view.findViewById(R.id.pager);
                    mPager.setAdapter(new ImageAdapter(getActivity(),images));
                    init(view);
                }
//                    playVideo(images.getJSONObject(0).getString("image_url"));
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                stream.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void sendMyBroadCast(String broadcastId, String video_id) {
        try {
            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(broadcastId);

            broadCastIntent.putExtra("media_id", video_id);
            broadCastIntent.putExtra("app_suffix", app_suffix);
            broadCastIntent.putExtra("app_id", app_id);
            broadCastIntent.putExtra("app_device_id", app_device_id);

            getActivity().sendBroadcast(broadCastIntent);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    Timer swipeTimer = null;
    private void init(View view) {



        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage >= images.length()) {
                    currentPage = 0;

                }
                try {
                    sendMyBroadCast(BroadcastNames.MEDIA_PLAYING.toString(),images.getJSONObject(currentPage).getString("media_id"));
                }catch (Exception e){
                    e.printStackTrace();
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        if(swipeTimer == null) {
            swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 2500, 2500);
        }
    }


    MyBroadCastReceiver myBroadCastReceiver;
    private void registerMyReceiver() {

        try
        {
            myBroadCastReceiver = new MyBroadCastReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("refresh_playlist_image");
            intentFilter.addAction(app_suffix);
            getActivity().registerReceiver(myBroadCastReceiver, intentFilter);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(swipeTimer!=null){
            swipeTimer.cancel();
        }
    }

    class MyBroadCastReceiver extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                Log.e("LOGGER", "Image BROADCAST RECEIVED = " + intent.getAction());

                    uiInit(rootView);



            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }


    /*class TapGestureListener extends GestureDetector.SimpleOnGestureListener{

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            // Your Code here
            Log.e("ImageFragment", "clicked fragment");
            if(settingsJSON.length()>0){
                if(settingsJSON.has("onclick")){
                    if(settingsJSON.has("url")){
                        new FragmentUtils().openApp(getActivity(),new MainActivity(),"http://www.google.com");
                    }
                }
            }
            return false;
        }
    }*/
}
