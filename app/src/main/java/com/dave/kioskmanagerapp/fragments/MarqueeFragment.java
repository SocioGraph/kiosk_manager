package com.dave.kioskmanagerapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dave.kioskmanagerapp.R;

import org.json.JSONArray;

public class MarqueeFragment extends Fragment {


    View rootView;
    JSONArray media_urls = new JSONArray();
    int count = 0;
    private String app_suffix = "";
    private String app_device_id = "";
    private String app_id = "";
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        try {
            rootView = inflater.inflate(R.layout.marquee_layout, container, false);
            Log.e("LOGGER", "VideoStream Opened");


            app_suffix = getArguments().getString("app_suffix");
            app_id = getArguments().getString("app_id");
            app_device_id = getArguments().getString("app_device_id");
            String vidAddress = getArguments().getString("url");
            media_urls = new JSONArray(vidAddress);
            String text_to_show = "";
            for(int i = 0 ; i < media_urls.length() ; i++) {
                if(i!=0){
                    text_to_show+=", ";
                }
                text_to_show+=media_urls.getString(i);
            }
            TextView tv = rootView.findViewById(R.id.marque_scrolling_text);
            tv.setText(text_to_show);
            tv.setSelected(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rootView;
    }
}
