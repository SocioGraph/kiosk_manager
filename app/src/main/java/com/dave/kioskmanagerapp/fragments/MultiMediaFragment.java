package com.dave.kioskmanagerapp.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dave.kioskmanagerapp.R;
import com.dave.kioskmanagerapp.utils.BroadcastNames;
import com.example.admin.daveai.others.DaveModels;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.analytics.AnalyticsListener;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

public class MultiMediaFragment extends ParentFragment implements AnalyticsListener {

    View rootView = null;

    private static final String TAG = "MultiMediaFragment";
    private PlayerView simpleExoPlayerView;
    private SimpleExoPlayer player;

    private ImageView imageView;
    DefaultBandwidthMeter bandwidthMeter;
    JSONArray playList = new JSONArray();
    int currentPlayIndex = 0;
    private static final int PERMISSION_REQUEST_CODE = 1;



    private String app_suffix = "";
    private String app_id = "";
    private String app_device_id = "";
    private String settingsString = "";
    private JSONObject settingsJSON = new JSONObject();


    @Override
    public View provideYourFragmentView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.multimedia_fragment, parent, false);
        Log.e("LOGGER","MultiMedia Fragment Opened");
        app_suffix = getArguments().getString("app_suffix");
        app_device_id = getArguments().getString("app_device_id");
        app_id = getArguments().getString("app_id");
        if(getArguments().containsKey("settings")){
            try {
                settingsString = getArguments().getString("settings");
                settingsJSON = new JSONObject(settingsString);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        uiInit(rootView);

        trackOnViewClicked(rootView,settingsJSON);


        return rootView;
    }

    @Override
    public boolean onMediaClicked() {
        return false;
    }


    public void setApp_suffix(String app_suffix) {
        this.app_suffix = app_suffix;
    }
    int conseqErrorCount = 0;

    private void uiInit(View view) {

//// I. ADJUST HERE:
////CHOOSE CONTENT: LiveStream / SdCard
//
////LIVE STREAM SOURCE: * Livestream links may be out of date so find any m3u8 files online and replace:
//
////        Uri mp4VideoUri =Uri.parse("http://81.7.13.162/hls/ss1/index.m3u8"); //random 720p source
////        Uri mp4VideoUri =Uri.parse("http://54.255.155.24:1935//Live/_definst_/amlst:sweetbcha1novD235L240P/playlist.m3u8"); //Radnom 540p indian channel
//        Uri mp4VideoUri =Uri.parse("http://cbsnewshd-lh.akamaihd.net/i/CBSNHD_7@199302/index_700_av-p.m3u8"); //CNBC
//        Uri mp4VideoUri =Uri.fromFile(new File("/storage/emulated/0/dave/cached_files/mens_fashion.mp4")); //ABC NEWS
////        Uri mp4VideoUri =Uri.parse("FIND A WORKING LINK ABD PLUg INTO HERE"); //PLUG INTO HERE<------------------------------------------
//
//


        imageView = view.findViewById(R.id.image);
        registerMyReceiver(app_suffix);
        bandwidthMeter = new DefaultBandwidthMeter(); //test
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        // 2. Create the player
        player = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector);
        simpleExoPlayerView = (PlayerView) view.findViewById(R.id.player_view);

        int h = simpleExoPlayerView.getResources().getConfiguration().screenHeightDp;
        int w = simpleExoPlayerView.getResources().getConfiguration().screenWidthDp;
        ////Set media controller
        simpleExoPlayerView.setUseController(false);//set to true or false to see controllers
        simpleExoPlayerView.requestFocus();
        // Bind the player to the view.
        simpleExoPlayerView.setPlayer(player);


        player.addListener(new Player.EventListener() {


            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                Log.v(TAG, "Listener-onTracksChanged... ");
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                Log.v(TAG, "Listener-onPlayerStateChanged..." + playbackState + "|||isDrawingCacheEnabled():" + simpleExoPlayerView.isDrawingCacheEnabled());
                if (playbackState == Player.STATE_ENDED) {
                    Log.e("LOGGER", "DONE PLAYING");
                    int lastIndex = 0;
                    if ((currentPlayIndex) == 0) {
                        lastIndex = playList.length() - 1;
                    } else {
                        lastIndex = currentPlayIndex - 1;
                    }
                    try {
                        playMedia(playList.getJSONObject(currentPlayIndex));
                        sendMyBroadCast(BroadcastNames.VIDEO_PLAYED.toString(), playList.getJSONObject(currentPlayIndex).toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if(playbackState == Player.STATE_READY){
                }
            }


            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                Log.v(TAG, "Listener-onPlayerError...");
                player.stop();
                player.setPlayWhenReady(true);
            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });
        currentPlayIndex = 0;
        player.setPlayWhenReady(true); //run file/link when ready to play.
        player.setVolume(0f);
        player.addAnalyticsListener(this);
        try {
            File yourFile = new File(Environment.getExternalStorageDirectory(), "/dave/cached_files/"+app_suffix+".json");
            FileInputStream stream = new FileInputStream(yourFile);
            String jsonStr = null;
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

                jsonStr = Charset.defaultCharset().decode(bb).toString();
                JSONObject jobj = new JSONObject(jsonStr);

                playList =jobj.getJSONArray("media");
                if (playList.length() > 0) {

                    sendMyBroadCast(BroadcastNames.MEDIA_PLAYING.toString(),playList.getJSONObject(0).getString("media_id"));
                    playMedia(playList.getJSONObject(0));
                }
                fc.close();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                stream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    private void playMedia(JSONObject jsonObject) {
        try {
            String filePath = jsonObject.getString("media_url");
            if (filePath.contains("http:") || filePath.contains("https:")) {
                conseqErrorCount++;
                if (conseqErrorCount > playList.length()) {
                    Toast.makeText(getActivity(), "Could not play media", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        if ((currentPlayIndex + 1) == playList.length()) {
                            currentPlayIndex = 0;
                        } else {
                            currentPlayIndex++;
                        }
                        playMedia(playList.getJSONObject(currentPlayIndex));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                conseqErrorCount = 0;
                try {
                    Uri mp4VideoUri = Uri.parse(filePath);
                    DaveModels daveModels = new DaveModels(getActivity(),true);
                    JSONObject mediaJSON = daveModels.getObject("media",playList.getJSONObject(currentPlayIndex).getString("media_id"),null,false,false);

                    String mediaType = mediaJSON.getString("media_type");
                    if ((currentPlayIndex + 1) == playList.length()) {
                        currentPlayIndex = 0;
                    } else {
                        currentPlayIndex++;
                    }
                    if(mediaType.equals("video")) {
                        simpleExoPlayerView.setVisibility(View.VISIBLE);
                        imageView.setVisibility(View.INVISIBLE);
                        // Measures bandwidth during playback. Can be null if not required.
                        // Produces DataSource instances through which media data is loaded.
                        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getActivity(), Util.getUserAgent(getActivity(), "exoplayer2example"), bandwidthMeter);

                        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

                        //This is the MediaSource representing the media to be played:
                        //FOR SD CARD SOURCE:
                        ExtractorMediaSource extractorMediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(mp4VideoUri);
//                MediaSource videoSource = new ExtractorMediaSource(mp4VideoUri, dataSourceFactory, extractorsFactory, null, null);

//        final LoopingMediaSource loopingSource = new LoopingMediaSource(videoSource);
                        // Prepare the player with the source.
                        player.prepare(extractorMediaSource);
                    }else if(mediaType.equals("image")){
                        showImage(filePath);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getActivity(),"Could not find media url in playlist",Toast.LENGTH_LONG).show();
        }
    }


    private void showImage(String filePath){
        simpleExoPlayerView.setVisibility(View.INVISIBLE);
        imageView.setVisibility(View.VISIBLE);
        Glide.with(getActivity()).load(filePath).into(imageView);
        startTimer(5000);
    }

    private void startTimer(long shutOffTime){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    playMedia(playList.getJSONObject(currentPlayIndex));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, shutOffTime);
    }
//-------------------------------------------------------ANDROID LIFECYCLE---------------------------------------------------------------------------------------------


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "onDestroy()...");
        player.release();

        getActivity().unregisterReceiver(myBroadCastReceiver);
    }

    MyBroadCastReceiver myBroadCastReceiver;

    private void registerMyReceiver(String app_suffix) {

        try {
            myBroadCastReceiver = new MyBroadCastReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(app_suffix);
            getActivity().registerReceiver(myBroadCastReceiver, intentFilter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onPlayerStateChanged(EventTime eventTime, boolean playWhenReady, int playbackState) {

    }

    @Override
    public void onTimelineChanged(EventTime eventTime, int reason) {

    }

    @Override
    public void onPositionDiscontinuity(EventTime eventTime, int reason) {

    }

    @Override
    public void onSeekStarted(EventTime eventTime) {

    }

    @Override
    public void onSeekProcessed(EventTime eventTime) {

    }

    @Override
    public void onPlaybackParametersChanged(EventTime eventTime, PlaybackParameters playbackParameters) {

    }

    @Override
    public void onRepeatModeChanged(EventTime eventTime, int repeatMode) {

    }

    @Override
    public void onShuffleModeChanged(EventTime eventTime, boolean shuffleModeEnabled) {

    }

    @Override
    public void onLoadingChanged(EventTime eventTime, boolean isLoading) {

    }

    @Override
    public void onPlayerError(EventTime eventTime, ExoPlaybackException error) {

    }

    @Override
    public void onTracksChanged(EventTime eventTime, TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadStarted(EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {

    }

    @Override
    public void onLoadCompleted(EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {

    }

    @Override
    public void onLoadCanceled(EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {

    }

    @Override
    public void onLoadError(EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData, IOException error, boolean wasCanceled) {

    }

    @Override
    public void onDownstreamFormatChanged(EventTime eventTime, MediaSourceEventListener.MediaLoadData mediaLoadData) {

    }

    @Override
    public void onUpstreamDiscarded(EventTime eventTime, MediaSourceEventListener.MediaLoadData mediaLoadData) {

    }

    @Override
    public void onMediaPeriodCreated(EventTime eventTime) {

    }

    @Override
    public void onMediaPeriodReleased(EventTime eventTime) {

    }

    @Override
    public void onReadingStarted(EventTime eventTime) {

    }

    @Override
    public void onBandwidthEstimate(EventTime eventTime, int totalLoadTimeMs, long totalBytesLoaded, long bitrateEstimate) {

    }

    @Override
    public void onViewportSizeChange(EventTime eventTime, int width, int height) {

    }

    @Override
    public void onNetworkTypeChanged(EventTime eventTime, @Nullable NetworkInfo networkInfo) {

    }

    @Override
    public void onMetadata(EventTime eventTime, Metadata metadata) {

    }

    @Override
    public void onDecoderEnabled(EventTime eventTime, int trackType, DecoderCounters decoderCounters) {

    }

    @Override
    public void onDecoderInitialized(EventTime eventTime, int trackType, String decoderName, long initializationDurationMs) {

    }

    @Override
    public void onDecoderInputFormatChanged(EventTime eventTime, int trackType, Format format) {

    }

    @Override
    public void onDecoderDisabled(EventTime eventTime, int trackType, DecoderCounters decoderCounters) {

    }

    @Override
    public void onAudioSessionId(EventTime eventTime, int audioSessionId) {

    }

    @Override
    public void onAudioUnderrun(EventTime eventTime, int bufferSize, long bufferSizeMs, long elapsedSinceLastFeedMs) {

    }

    @Override
    public void onDroppedVideoFrames(EventTime eventTime, int droppedFrames, long elapsedMs) {

    }

    @Override
    public void onVideoSizeChanged(EventTime eventTime, int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {

    }

    @Override
    public void onRenderedFirstFrame(EventTime eventTime, Surface surface) {

    }

    @Override
    public void onDrmKeysLoaded(EventTime eventTime) {

    }

    @Override
    public void onDrmSessionManagerError(EventTime eventTime, Exception error) {

    }

    @Override
    public void onDrmKeysRestored(EventTime eventTime) {

    }

    @Override
    public void onDrmKeysRemoved(EventTime eventTime) {

    }


    class MyBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                Log.e("LOGGER", "Multimedia BROADCAST RECEIVED = " + intent.getAction());
                if (intent.getAction().equals(app_suffix)) {
                    try {
                        uiInit(rootView);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void sendMyBroadCast(String broadcastId, String video_id) {
        try {
            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(broadcastId);

            broadCastIntent.putExtra("media_id", video_id);
            broadCastIntent.putExtra("app_suffix", app_suffix);
            broadCastIntent.putExtra("app_id", app_id);
            broadCastIntent.putExtra("app_device_id", app_device_id);

            getActivity().sendBroadcast(broadCastIntent);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
