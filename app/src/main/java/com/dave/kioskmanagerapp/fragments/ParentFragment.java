package com.dave.kioskmanagerapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dave.kioskmanagerapp.MainActivity;
import com.dave.kioskmanagerapp.utils.FragmentUtils;

import org.json.JSONObject;

public abstract class ParentFragment extends Fragment {

    private JSONObject settingsJSON = new JSONObject();

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanseState)
    {
        View view = provideYourFragmentView(inflater,parent,savedInstanseState);
        return view;
    }

    public abstract View provideYourFragmentView(LayoutInflater inflater,ViewGroup parent, Bundle savedInstanceState);

    public void trackOnViewClicked(View view, JSONObject settingsJSON){
        this.settingsJSON = settingsJSON;
        GestureDetectorCompat tapGestureDetector = new GestureDetectorCompat(getActivity(), new TapGestureListener());


        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                tapGestureDetector.onTouchEvent(event);
                return false;
            }
        });
    }


    public abstract boolean onMediaClicked();


    class TapGestureListener extends GestureDetector.SimpleOnGestureListener{

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            // Your Code here
            Log.e("ImageFragment", "clicked fragment");
            try {
                if(onMediaClicked()){

                }else {
                    if (settingsJSON.length() > 0) {
                        if (settingsJSON.has("onClick") && settingsJSON.getString("onClick").equals("webview")) {
                            if (settingsJSON.has("url")) {
                                if (settingsJSON.has("shutOffTime")) {
                                    new FragmentUtils().openApp(getActivity(), new MainActivity(), settingsJSON.getString("url"), settingsJSON.getInt("shutOffTime"));
                                }
                            }
                        } else if (settingsJSON.has("onClick") && settingsJSON.getString("onClick").equals("app")) {
                            if (settingsJSON.has("app_id")) {
                                if (settingsJSON.has("shutOffTime")) {
//                                new FragmentUtils().openOtherApp(getActivity(),"com.visualiser.dave.sociograph.davevisualiser",30);
                                    new FragmentUtils().openOtherApp(getActivity(), settingsJSON.getString("app_id"), settingsJSON.getInt("shutOffTime"));
                                }
                            }
                        } else if (settingsJSON.has("onClick") && settingsJSON.getString("onClick").equals("map")) {
                            //TODO to be implemented
                        }
                    }
                }

            }catch (Exception exception){
                exception.printStackTrace();
                Toast.makeText(getActivity(),"Error reading onClick of Fragment Action",Toast.LENGTH_LONG).show();
            }
            return false;
        }
    }

}
