package com.dave.kioskmanagerapp.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dave.kioskmanagerapp.MainActivity;
import com.dave.kioskmanagerapp.R;
import com.dave.kioskmanagerapp.utils.BroadcastNames;
import com.dave.kioskmanagerapp.utils.FragmentUtils;
import com.example.admin.daveai.others.DaveModels;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.analytics.AnalyticsListener;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

public class VideoFragment extends ParentFragment implements AnalyticsListener {
    View rootView = null;

    private static final String TAG = "VideoActivity";
    private PlayerView simpleExoPlayerView;
    private SimpleExoPlayer player;
    DefaultBandwidthMeter bandwidthMeter;
    JSONArray playList = new JSONArray();
    int currentPlayIndex = 0;
    private static final int PERMISSION_REQUEST_CODE = 1;



    private String app_suffix = "";
    private String app_id = "";
    private String app_device_id = "";
    private String settingsString = "";
    private JSONObject settingsJSON = new JSONObject();


    @Override
    public View provideYourFragmentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.video_fragment, container, false);
        Log.e("LOGGER","Video Fragment Opened");
        app_suffix = getArguments().getString("app_suffix");
        app_device_id = getArguments().getString("app_device_id");
        app_id = getArguments().getString("app_id");
        if(getArguments().containsKey("settings")){
            try {
                settingsString = getArguments().getString("settings");
                settingsJSON = new JSONObject(settingsString);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        uiInit(rootView);

        trackOnViewClicked(rootView,settingsJSON);


        return rootView;
    }

    @Override
    public boolean onMediaClicked() {
        Log.e("LOGGER","CLICKED IT");
        try {
            Log.e("LOGGER", "clicked on media id = "+playList.getJSONObject(currentPlayIndex).getString("media_id"));

            DaveModels daveModels = new DaveModels(getActivity(),true);
            JSONObject mediajson = daveModels.getObject("media", playList.getJSONObject(currentPlayIndex).getString("media_id"));
            JSONObject jobj = new JSONObject();
            if(mediajson.has("settings")){
                jobj = mediajson.getJSONObject("settings");
            }
            Log.e("LOGGER","MEDIA DATA!!!!!!!"+jobj);
            if (jobj.length() > 0) {
                if (jobj.has("onClick") && jobj.getString("onClick").equals("webview")) {
                    if (jobj.has("url")) {
                        if (jobj.has("shutOffTime")) {
                            new FragmentUtils().openApp(getActivity(), new MainActivity(), jobj.getString("url"), jobj.getInt("shutOffTime"));
                        }
                    }
                } else if (jobj.has("onClick") && jobj.getString("onClick").equals("app")) {
                    if (jobj.has("app_id")) {
                        if (jobj.has("shutOffTime")) {
//                                new FragmentUtils().openOtherApp(getActivity(),"com.visualiser.dave.sociograph.davevisualiser",30);
                            new FragmentUtils().openOtherApp(getActivity(), jobj.getString("app_id"), jobj.getInt("shutOffTime"));
                        }
                    }
                } else if (jobj.has("onClick") && jobj.getString("onClick").equals("map")) {
                    //TODO to be implemented
                }
            }


        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }


    public void setApp_suffix(String app_suffix) {
        this.app_suffix = app_suffix;
    }





    private void sendMyBroadCast(String broadcastId, String video_id) {
        try {
            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(broadcastId);

            broadCastIntent.putExtra("media_id", video_id);
            broadCastIntent.putExtra("app_suffix", app_suffix);
            broadCastIntent.putExtra("app_id", app_id);
            broadCastIntent.putExtra("app_device_id", app_device_id);

            getActivity().sendBroadcast(broadCastIntent);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private void uiInit(View view) {

//// I. ADJUST HERE:
////CHOOSE CONTENT: LiveStream / SdCard
//
////LIVE STREAM SOURCE: * Livestream links may be out of date so find any m3u8 files online and replace:
//
////        Uri mp4VideoUri =Uri.parse("http://81.7.13.162/hls/ss1/index.m3u8"); //random 720p source
////        Uri mp4VideoUri =Uri.parse("http://54.255.155.24:1935//Live/_definst_/amlst:sweetbcha1novD235L240P/playlist.m3u8"); //Radnom 540p indian channel
//        Uri mp4VideoUri =Uri.parse("http://cbsnewshd-lh.akamaihd.net/i/CBSNHD_7@199302/index_700_av-p.m3u8"); //CNBC
//        Uri mp4VideoUri =Uri.fromFile(new File("/storage/emulated/0/dave/cached_files/mens_fashion.mp4")); //ABC NEWS
////        Uri mp4VideoUri =Uri.parse("FIND A WORKING LINK ABD PLUg INTO HERE"); //PLUG INTO HERE<------------------------------------------
//
//


        registerMyReceiver(app_suffix);
        bandwidthMeter = new DefaultBandwidthMeter(); //test
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        // 2. Create the player
        player = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector);
        simpleExoPlayerView = (PlayerView) view.findViewById(R.id.player_view);

        int h = simpleExoPlayerView.getResources().getConfiguration().screenHeightDp;
        int w = simpleExoPlayerView.getResources().getConfiguration().screenWidthDp;
        ////Set media controller
        simpleExoPlayerView.setUseController(false);//set to true or false to see controllers
        simpleExoPlayerView.requestFocus();
        // Bind the player to the view.
        simpleExoPlayerView.setPlayer(player);


        player.addListener(new Player.EventListener() {


            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                Log.v(TAG, "Listener-onTracksChanged... ");
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                Log.v(TAG, "Listener-onPlayerStateChanged..." + playbackState + "|||isDrawingCacheEnabled():" + simpleExoPlayerView.isDrawingCacheEnabled());
                if (playbackState == Player.STATE_ENDED) {
                    Log.e("LOGGER", "DONE PLAYING");
                    int lastIndex = 0;
                    if ((currentPlayIndex) == 0) {
                        lastIndex = playList.length() - 1;
                    } else {
                        lastIndex = currentPlayIndex - 1;
                    }
                    try {
                        playVideo(playList.getJSONObject(currentPlayIndex).getString("media_url"));
                        sendMyBroadCast(BroadcastNames.VIDEO_PLAYED.toString(), playList.getJSONObject(currentPlayIndex).toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if(playbackState == Player.STATE_READY){
                }
            }


            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                Log.v(TAG, "Listener-onPlayerError...");
                player.stop();
                player.setPlayWhenReady(true);
            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });
        currentPlayIndex = 0;
        player.setPlayWhenReady(true); //run file/link when ready to play.
        player.setVolume(0f);
        player.addAnalyticsListener(this);
        try {
            File yourFile = new File(Environment.getExternalStorageDirectory(), "/dave/cached_files/"+app_suffix+".json");
            FileInputStream stream = new FileInputStream(yourFile);
            String jsonStr = null;
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

                jsonStr = Charset.defaultCharset().decode(bb).toString();
                JSONObject jobj = new JSONObject(jsonStr);

                playList =jobj.getJSONArray("media");
                if (playList.length() > 0) {

                    sendMyBroadCast(BroadcastNames.MEDIA_PLAYING.toString(),playList.getJSONObject(0).getString("media_id"));
                    playVideo(playList.getJSONObject(0).getString("media_url"));
                }
                fc.close();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                stream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    int conseqErrorCount = 0;
    private void playVideo(String filePath) {
        if (filePath.contains("http:")||filePath.contains("https:")) {
            conseqErrorCount++;
            if(conseqErrorCount > playList.length()){
                Toast.makeText(getActivity(),"Could not play videos",Toast.LENGTH_SHORT).show();
            }else {
//            Log.e("LOGGER", "problem with file path = " + filePath);
                try {
                    if ((currentPlayIndex + 1) == playList.length()) {
                        currentPlayIndex = 0;
                    } else {
                        currentPlayIndex++;
                    }
                    playVideo(playList.getJSONObject(currentPlayIndex).getString("media_url"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {conseqErrorCount=0;
            try {
                Uri mp4VideoUri = Uri.parse(filePath);
                if ((currentPlayIndex + 1) == playList.length()) {
                    currentPlayIndex = 0;
                } else {
                    currentPlayIndex++;
                }
                // Measures bandwidth during playback. Can be null if not required.
                // Produces DataSource instances through which media data is loaded.
                DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getActivity(), Util.getUserAgent(getActivity(), "exoplayer2example"), bandwidthMeter);

                ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

                //This is the MediaSource representing the media to be played:
                //FOR SD CARD SOURCE:
                ExtractorMediaSource extractorMediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(mp4VideoUri);
//                MediaSource videoSource = new ExtractorMediaSource(mp4VideoUri, dataSourceFactory, extractorsFactory, null, null);

//        final LoopingMediaSource loopingSource = new LoopingMediaSource(videoSource);
                // Prepare the player with the source.
                player.prepare(extractorMediaSource);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

//-------------------------------------------------------ANDROID LIFECYCLE---------------------------------------------------------------------------------------------


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "onDestroy()...");
        player.release();

        getActivity().unregisterReceiver(myBroadCastReceiver);
    }

    @Override
    public void onPlayerStateChanged(EventTime eventTime, boolean playWhenReady, int playbackState) {

    }

    @Override
    public void onTimelineChanged(EventTime eventTime, int reason) {

    }

    @Override
    public void onPositionDiscontinuity(EventTime eventTime, int reason) {

    }

    @Override
    public void onSeekStarted(EventTime eventTime) {

    }

    @Override
    public void onSeekProcessed(EventTime eventTime) {

    }

    @Override
    public void onPlaybackParametersChanged(EventTime eventTime, PlaybackParameters playbackParameters) {

    }

    @Override
    public void onRepeatModeChanged(EventTime eventTime, int repeatMode) {

    }

    @Override
    public void onShuffleModeChanged(EventTime eventTime, boolean shuffleModeEnabled) {

    }

    @Override
    public void onLoadingChanged(EventTime eventTime, boolean isLoading) {

    }

    @Override
    public void onPlayerError(EventTime eventTime, ExoPlaybackException error) {

    }

    @Override
    public void onTracksChanged(EventTime eventTime, TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadStarted(EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {

    }

    @Override
    public void onLoadCompleted(EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {

    }

    @Override
    public void onLoadCanceled(EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {

    }

    @Override
    public void onLoadError(EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData, IOException error, boolean wasCanceled) {

    }

    @Override
    public void onDownstreamFormatChanged(EventTime eventTime, MediaSourceEventListener.MediaLoadData mediaLoadData) {

    }

    @Override
    public void onUpstreamDiscarded(EventTime eventTime, MediaSourceEventListener.MediaLoadData mediaLoadData) {

    }

    @Override
    public void onMediaPeriodCreated(EventTime eventTime) {

    }

    @Override
    public void onMediaPeriodReleased(EventTime eventTime) {

    }

    @Override
    public void onReadingStarted(EventTime eventTime) {

    }

    @Override
    public void onBandwidthEstimate(EventTime eventTime, int totalLoadTimeMs, long totalBytesLoaded, long bitrateEstimate) {

    }

    @Override
    public void onViewportSizeChange(EventTime eventTime, int width, int height) {

    }

    @Override
    public void onNetworkTypeChanged(EventTime eventTime, @Nullable NetworkInfo networkInfo) {

    }

    @Override
    public void onMetadata(EventTime eventTime, Metadata metadata) {

    }

    @Override
    public void onDecoderEnabled(EventTime eventTime, int trackType, DecoderCounters decoderCounters) {

    }

    @Override
    public void onDecoderInitialized(EventTime eventTime, int trackType, String decoderName, long initializationDurationMs) {

    }

    @Override
    public void onDecoderInputFormatChanged(EventTime eventTime, int trackType, Format format) {

    }

    @Override
    public void onDecoderDisabled(EventTime eventTime, int trackType, DecoderCounters decoderCounters) {

    }

    @Override
    public void onAudioSessionId(EventTime eventTime, int audioSessionId) {

    }

    @Override
    public void onAudioUnderrun(EventTime eventTime, int bufferSize, long bufferSizeMs, long elapsedSinceLastFeedMs) {

    }

    @Override
    public void onDroppedVideoFrames(EventTime eventTime, int droppedFrames, long elapsedMs) {

    }

    @Override
    public void onVideoSizeChanged(EventTime eventTime, int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {

    }

    @Override
    public void onRenderedFirstFrame(EventTime eventTime, Surface surface) {

    }

    @Override
    public void onDrmKeysLoaded(EventTime eventTime) {

    }

    @Override
    public void onDrmSessionManagerError(EventTime eventTime, Exception error) {

    }

    @Override
    public void onDrmKeysRestored(EventTime eventTime) {

    }

    @Override
    public void onDrmKeysRemoved(EventTime eventTime) {

    }


    MyBroadCastReceiver myBroadCastReceiver;

    private void registerMyReceiver(String app_suffix) {

        try {
            myBroadCastReceiver = new MyBroadCastReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(app_suffix);
            getActivity().registerReceiver(myBroadCastReceiver, intentFilter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    class MyBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                Log.e("LOGGER", "Video BROADCAST RECEIVED = " + intent.getAction());
                if (intent.getAction().equals(app_suffix)) {
                    try {
                        uiInit(rootView);
                        /*File yourFile = new File(Environment.getExternalStorageDirectory(), "/dave/cached_files/video.json");
                        FileInputStream stream = new FileInputStream(yourFile);
                        String jsonStr = null;
                        try {
                            FileChannel fc = stream.getChannel();
                            MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

                            jsonStr = Charset.defaultCharset().decode(bb).toString();
                            JSONObject jobj = new JSONObject(jsonStr);
                            playList = jobj.getJSONArray("data");
                            currentPlayIndex = 0;
                            if (player.getPlayWhenReady()) {
                                player.stop();
                            }
                            if (playList.length() > 0)
                                playVideo(playList.getJSONObject(0).getString("media_url"));

                            fc.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            stream.close();
                        }*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

}
