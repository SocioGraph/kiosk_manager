package com.dave.kioskmanagerapp.fragments;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.dave.kioskmanagerapp.R;
import android.net.Uri;
import android.widget.MediaController;
import android.widget.VideoView;
import org.json.JSONArray;

public class VideoStreamerFragment  extends Fragment {


    View rootView;
    JSONArray media_urls = new JSONArray();
    int count = 0;
    private String app_suffix = "";
    private String app_device_id = "";
    private String app_id = "";
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        try {
            rootView = inflater.inflate(R.layout.video_streaming_fragment, container, false);
            Log.e("LOGGER", "VideoStream Opened");


            app_suffix = getArguments().getString("app_suffix");
            app_id = getArguments().getString("app_id");
            app_device_id = getArguments().getString("app_device_id");
            String vidAddress = getArguments().getString("url");
            media_urls = new JSONArray(vidAddress);
            Log.e("LOGGER", "VideoStream Opened "+media_urls.toString());
            VideoView vidView = (VideoView)rootView.findViewById(R.id.myVideo);
            Uri vidUri = Uri.parse(media_urls.getString(count++));
            vidView.setVideoURI(vidUri);

            vidView.start();
            vidView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    try {
                        Log.e("LOGGER", "VIDEO ENDED");

                        Uri vidUri = Uri.parse(media_urls.getString(count++));
                        if (count % (media_urls.length()) == 0) {
                            count = 0;
                        }
                        vidView.setVideoURI(vidUri);
                        vidView.start();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        return rootView;
    }


}