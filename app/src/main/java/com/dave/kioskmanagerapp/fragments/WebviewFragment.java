package com.dave.kioskmanagerapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.dave.kioskmanagerapp.R;
import com.example.admin.daveai.others.DaveSharedPreference;

import org.json.JSONArray;

import java.util.HashMap;

public class WebviewFragment extends Fragment {

    JSONArray images = new JSONArray();
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static final int PERMISSION_REQUEST_CODE = 12;
    View rootView;
    WebView webView;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        try {
            rootView = inflater.inflate(R.layout.webview_fragment, container, false);
            Log.e("LOGGER", "Webview Opened");
            webView = rootView.findViewById(R.id.webView);

            webView.getSettings().setJavaScriptEnabled(true);
            webView.setWebViewClient(new WebViewController());

            app_suffix = getArguments().getString("app_suffix");
            app_id = getArguments().getString("app_id");
            app_device_id = getArguments().getString("app_device_id");
            String url = getArguments().getString("url");
            /*HashMap<String, String> headers = new HashMap();

            DaveSharedPreference sharedPreference = new DaveSharedPreference(getActivity());
            headers.put("X-I2CE-API-KEY", sharedPreference.readString(DaveSharedPreference.API_KEY));
            headers.put("X-I2CE-USER-ID", sharedPreference.readString(DaveSharedPreference.USER_ID));
            headers.put("X-I2CE-ENTERPRISE-ID", sharedPreference.readString(DaveSharedPreference.EnterpriseId));*/
            webView.loadUrl(url);

        }catch (Exception e){
            e.printStackTrace();
        }
        return rootView;
    }
    private String app_suffix = "";
    private String app_device_id = "";
    private String app_id = "";


    public class WebViewController extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}