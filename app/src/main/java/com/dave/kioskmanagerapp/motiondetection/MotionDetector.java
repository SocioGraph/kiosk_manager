package com.dave.kioskmanagerapp.motiondetection;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.bumptech.glide.util.Util;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class MotionDetector {
    private long MOTION_DETECTION_FRAME_INTERVAL = 150;
    private int threshPercent = 25;
    private int threshHold = 50;
    private int FACE_DETECT_FPS_DIVIDER = 5;
    private int minBoundingBoxPercent = 1;
    private int maxBoundingBoxPercent = 75;
    private long checkInterval = 500;

    private boolean faceAvailbleAlready = false;

    private int frameFaceDetectedCount = 0;

    public boolean isFaceAvailbleAlready() {

        return faceAvailbleAlready;
    }

    public void setFaceAvailbleAlready(boolean faceAvailbleAlready) {
        if(faceAvailbleAlready!=this.faceAvailbleAlready) {
            frameFaceDetectedCount = 0;
        }
        this.faceAvailbleAlready = faceAvailbleAlready;

    }

    public int getThreshPercent() {
        return threshPercent;
    }

    public void setThreshPercent(int threshPercent) {
        this.threshPercent = threshPercent;
    }

    public int getMinBoundingBoxPercent() {
        return minBoundingBoxPercent;
    }

    public void setMinBoundingBoxPercent(int minBoundingBoxPercent) {
        this.minBoundingBoxPercent = minBoundingBoxPercent;
    }

    public int getMaxBoundingBoxPercent() {
        return maxBoundingBoxPercent;
    }

    public void setMaxBoundingBoxPercent(int maxBoundingBoxPercent) {
        this.maxBoundingBoxPercent = maxBoundingBoxPercent;
    }

    class MotionDetectorThread extends Thread {
        private AtomicBoolean isRunning = new AtomicBoolean(true);
        private Mat lastImageGrey = null;
        public void stopDetection() {
            isRunning.set(false);
        }

        @Override
        public void run() {
            while (isRunning.get()) {
                try {
                    long now = System.currentTimeMillis();
                    if (now - lastCheck > checkInterval) {
                        lastCheck = now;
                        if (nextData.get() != null) {

                            Camera.Parameters parameters = mCamera.getParameters();
                            int width = parameters.getPreviewSize().width;
                            int height = parameters.getPreviewSize().height;


                            YuvImage yuv = new YuvImage(nextData.get(), parameters.getPreviewFormat(), nextWidth.get(), nextHeight.get(), null);
                            ByteArrayOutputStream out = new ByteArrayOutputStream();
                            yuv.compressToJpeg(new android.graphics.Rect(0, 0, width, height), 70, out);
                            Bitmap bmp = BitmapFactory.decodeByteArray(out.toByteArray(), 0, out.size());

//convert Bitmap to Mat; note the bitmap config ARGB_8888 conversion that
//allows you to use other image processing methods and still save at the end
                            Mat origImage = new Mat();
                            bmp = bmp.copy(Bitmap.Config.ARGB_8888, true);
                            Utils.bitmapToMat(bmp, origImage);


                            //create greyscale image
                            Mat hierarchy = new Mat();
                            Mat matGrey = new Mat();
                            Imgproc.cvtColor(origImage, matGrey, Imgproc.COLOR_BGR2GRAY);

                            if (lastImageGrey != null) {

                                List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
                                int top = matGrey.height();
                                int left = matGrey.width();
                                int right = 0;
                                int bottom = 0;


                                Mat absDiffMat = new Mat();
                                //get absolute difference between current and previous frame
                                Core.absdiff(matGrey, lastImageGrey, absDiffMat);

                                Core.MinMaxLocResult absDiffMinMaxResult = Core.minMaxLoc(absDiffMat);

                                Imgproc.threshold(absDiffMat, absDiffMat, threshHold/*absDiffMinMaxResult.maxVal*(threshPercent/100)*/, 255, Imgproc.THRESH_BINARY);

                                Imgproc.GaussianBlur(absDiffMat, absDiffMat, new Size(9, 9), 2, 2);
                                Imgproc.findContours(absDiffMat, contours, hierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

                                ArrayList<Rect> listOfDetectedMotion = new ArrayList<>();

                                for (int contourIdx = 0; contourIdx < contours.size(); contourIdx++) {
                                    // Minimum size allowed for consideration
                                    MatOfPoint2f approxCurve = new MatOfPoint2f();
                                    MatOfPoint2f contour2f = new MatOfPoint2f(contours.get(contourIdx).toArray());
                                    //Processing on mMOP2f1 which is in type MatOfPoint2f
                                    double approxDistance = Imgproc.arcLength(contour2f, true) * 0.02;
                                    Imgproc.approxPolyDP(contour2f, approxCurve, approxDistance, true);

                                    //Convert back to MatOfPoint
                                    MatOfPoint points = new MatOfPoint(approxCurve.toArray());

                                    // Get bounding rect of contour
                                    Rect rect = Imgproc.boundingRect(points);
                                    if ((rect.height * rect.width) > ((origImage.height() * origImage.width() * minBoundingBoxPercent) / 100) && (rect.height * rect.width) < ((origImage.height() * origImage.width() * maxBoundingBoxPercent) / 100)) {
//                                    Imgproc.drawContours(origImage, contours, contourIdx, new Scalar(255, 255, 255), 5);
//                                    Imgproc.rectangle(origImage, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(255, 0, 0, 255), 3);
                                        listOfDetectedMotion.add(rect);
                                    }


                                }


//                                Core.rotate(origImage, origImage, Core.ROTATE_90_CLOCKWISE);
                                Bitmap bmpFinal = Bitmap.createBitmap(origImage.width(), origImage.height(), Bitmap.Config.ARGB_8888);
                                Utils.matToBitmap(origImage, bmpFinal);

                                lastImageGrey = matGrey;
                                if(faceAvailbleAlready){
                                    frameFaceDetectedCount++;
                                }
                                if(faceAvailbleAlready && frameFaceDetectedCount == FACE_DETECT_FPS_DIVIDER) {
                                    frameFaceDetectedCount = 0;
                                    mHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            motionDetectorCallback.onMotionDetected(bmpFinal,faceAvailbleAlready);
                                        }
                                    });
                                }else if (listOfDetectedMotion.size() > 0) {
                                    mHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            motionDetectorCallback.onMotionDetected(bmpFinal,faceAvailbleAlready);
                                        }
                                    });
                                }
                            } else {
                                lastImageGrey = matGrey;
                            }

                        }
                    }
                    try {
                        Thread.sleep(MOTION_DETECTION_FRAME_INTERVAL);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    private final AggregateLumaMotionDetection detector;
    private long lastCheck = 0;
    private MotionDetectorCallback motionDetectorCallback;
    private Handler mHandler = new Handler();

    private AtomicReference<byte[]> nextData = new AtomicReference<>();
    private AtomicInteger nextWidth = new AtomicInteger();
    private AtomicInteger nextHeight = new AtomicInteger();
    private int minLuma = 1000;
    private MotionDetectorThread worker;

    private Camera mCamera;
    private boolean inPreview;
    private SurfaceHolder previewHolder;
    private Context mContext;
    private SurfaceView mSurface;

    public MotionDetector(Context context, SurfaceView previewSurface) {
        detector = new AggregateLumaMotionDetection();
        mContext = context;
        mSurface = previewSurface;
    }

    public void setMotionDetectorCallback(MotionDetectorCallback motionDetectorCallback) {
        this.motionDetectorCallback = motionDetectorCallback;
    }

    public void consume(byte[] data, int width, int height) {
        nextData.set(data);
        nextWidth.set(width);
        nextHeight.set(height);
    }

    public void setCheckInterval(long checkInterval) {
        this.checkInterval = checkInterval;
    }

    public void setMinLuma(int minLuma) {
        this.minLuma = minLuma;
    }

    public void setLeniency(int l) {
        detector.setLeniency(l);
    }

    public void onResume() {
        if (checkCameraHardware()) {
            mCamera = getCameraInstance();
            mCamera = Camera.open();
            Camera.Parameters params = mCamera.getParameters();

// Check what resolutions are supported by your camera
            List<Camera.Size> sizes = params.getSupportedPictureSizes();

// Iterate through all available resolutions and choose one.
// The chosen resolution will be stored in mSize.

            for (Camera.Size size : sizes) {

                Log.i("CAMERA SIZE", "Available resolution: " + size.width + " " + size.height);
                if(size.width<1201) {
                    params.setPictureSize(size.width, size.height);
                    mCamera.setParameters(params);
                    break;
                }
            }
            if(OpenCVLoader.initDebug()){
                Log.e("LOGGER","OPEN CV LOADED");
            }else{
                Log.e("LOGGER","OPEN CV NOT LOADED");
            }
            worker = new MotionDetectorThread();
            worker.start();

            // configure preview
            previewHolder = mSurface.getHolder();
            previewHolder.addCallback(surfaceCallback);
            previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }
    }

    public boolean checkCameraHardware() {
        if (mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    private Camera getCameraInstance(){
        Camera c = null;

        try {
            if (Camera.getNumberOfCameras() >= 2) {
                //if you want to open front facing camera use this line
                c = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
            } else {
                c = Camera.open();
            }

        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
            //txtStatus.setText("Kamera nicht zur Benutzung freigegeben");
        }
        return c; // returns null if camera is unavailable
    }

    private Camera.PreviewCallback previewCallback = new Camera.PreviewCallback() {

        /**
         * {@inheritDoc}
         */
        @Override
        public void onPreviewFrame(byte[] data, Camera cam) {
            if (data == null) return;
            Camera.Size size = cam.getParameters().getPreviewSize();
            if (size == null) return;
            consume(data, size.width, size.height);
        }
    };


    private SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {

        /**
         * {@inheritDoc}
         */
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                mCamera.setPreviewDisplay(previewHolder);
                mCamera.setPreviewCallback(previewCallback);
            } catch (Throwable t) {
                Log.e("MotionDetector", "Exception in setPreviewDisplay()", t);
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Camera.Parameters parameters = mCamera.getParameters();
            Camera.Size size = getBestPreviewSize(width, height, parameters);
            if (size != null) {
                parameters.setPreviewSize(size.width, size.height);
                Log.d("MotionDetector", "Using width=" + size.width + " height=" + size.height);
            }
            mCamera.setParameters(parameters);
            mCamera.startPreview();
            inPreview = true;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // Ignore
        }
    };

    private static Camera.Size getBestPreviewSize(int width, int height, Camera.Parameters parameters) {
        Camera.Size result = null;

        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    int resultArea = result.width * result.height;
                    int newArea = size.width * size.height;

                    if (newArea > resultArea) result = size;
                }
            }
        }

        return result;
    }

    public void onPause() {
        releaseCamera();
        if (previewHolder != null) previewHolder.removeCallback(surfaceCallback);
        if (worker != null) worker.stopDetection();
    }

    private void releaseCamera(){
        if (mCamera != null){
            mCamera.setPreviewCallback(null);
            if (inPreview) mCamera.stopPreview();
            inPreview = false;
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }
}
