package com.dave.kioskmanagerapp.motiondetection;

import android.graphics.Bitmap;

public interface MotionDetectorCallback {
    void onMotionDetected(Bitmap imgBytes, boolean faceAlreadyDetected);
    void onMotionDetected();
    void onTooDark();
}
