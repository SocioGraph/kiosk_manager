package com.dave.kioskmanagerapp.ocv_detection;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.media.Image;
import android.os.Environment;

import com.dave.kioskmanagerapp.ocv_detection.listeners.OnFaceDetectedListener;
import com.dave.kioskmanagerapp.ocv_detection.utils.OpenCVUtils;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Random;

import static org.opencv.imgcodecs.Imgcodecs.imread;

public class FaceDetector {



    private CascadeClassifier cascadeClassifier;


    public void processImage(Bitmap image, Context context, OnFaceDetectedListener onFaceDetectedListener, boolean randomiser){
        if(randomiser){
            Random random = new Random();

            if(random.nextBoolean()){
                int height=image.getHeight();
                int width=image.getWidth();
                int x;
                int y;
                Random r=new Random();
                x=r.nextInt(width);
                y=r.nextInt(height);
                Rect rect = new Rect(x,y,(width-x)/2,(height/y)/2);
                Rect[] rects = new Rect[]{rect};
                onFaceDetectedListener.onFaceDetected( rects);
            }else{
                onFaceDetectedListener.onFaceDetected(new Rect[0]);
            }
        }else {
            OpenCVUtils openCVUtils = new OpenCVUtils(context);


            cascadeClassifier = openCVUtils.getClassifier(OpenCVUtils.Classifier.FACE_DETECTOR);
            //Process image data

                try {

                    Mat grayscaleImage;
                    int absoluteFaceSize;
                    grayscaleImage = new Mat();

                    // The faces will be a 20% of the height of the screen
                    absoluteFaceSize = (int) (image.getHeight() * 0.2);
                    Mat mat = new Mat();
                    Utils.bitmapToMat(image,mat);
                    Imgproc.cvtColor(mat, grayscaleImage, Imgproc.COLOR_RGBA2RGB);
                    MatOfRect faces = new MatOfRect();

                    // Use the classifier to detect faces
                    if (cascadeClassifier != null) {
                        cascadeClassifier.detectMultiScale(grayscaleImage, faces, 1.1, 2, 2,
                                new org.opencv.core.Size(absoluteFaceSize, absoluteFaceSize), new org.opencv.core.Size());
                    }

                    // If there are any faces found, draw a rectangle around it
                    Rect[] facesArray = faces.toArray();
                    onFaceDetectedListener.onFaceDetected(facesArray);
                } catch (Exception e) {
                    e.printStackTrace();
                    onFaceDetectedListener.onError(e.getMessage(), 1);
                }

//            new DetectFace().execute();


        }

    }


}
