package com.dave.kioskmanagerapp.ocv_detection;

import android.content.Context;
import android.media.Image;

import com.dave.kioskmanagerapp.ocv_detection.listeners.OnFullBodyDetectedListener;
import com.dave.kioskmanagerapp.ocv_detection.utils.OpenCVUtils;

import org.opencv.core.Rect;
import org.opencv.objdetect.CascadeClassifier;

import java.util.Random;

public class FullBodyDetector {



    private CascadeClassifier cascadeClassifier;


    public void processImage(Image image, Context context, OnFullBodyDetectedListener onFullBodyDetectedListener, boolean randomised){
        OpenCVUtils openCVUtils = new OpenCVUtils(context);


        cascadeClassifier = openCVUtils.getClassifier(OpenCVUtils.Classifier.FULL_BODY_DETECTOR);

        if(randomised){
            Random random = new Random();

            if(random.nextBoolean()){
                int height=image.getHeight();
                int width=image.getWidth();
                int x;
                int y;
                Random r=new Random();
                x=r.nextInt(width);
                y=r.nextInt(height);
                Rect rect = new Rect(x,y,(width-x)/2,(height/y)/2);
                Rect[] rects = new Rect[]{rect};
                onFullBodyDetectedListener.onFullBodyDetected(true, rects);
            }else{
                onFullBodyDetectedListener.onFullBodyDetected(false,new Rect[0]);
            }
        }



    }



}


