package com.dave.kioskmanagerapp.ocv_detection;

import android.graphics.ImageFormat;
import android.media.Image;
import android.os.Environment;
import android.util.Log;

import com.dave.kioskmanagerapp.ocv_detection.listeners.OnHumanDetectedListener;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.HOGDescriptor;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.util.List;

import static org.opencv.imgcodecs.Imgcodecs.imread;

public class HumanDetector {

    private static final String TAG ="HumanDetector.class";

    private HOGDescriptor hogDescriptor;
    final MatOfRect locations = new MatOfRect();
    final MatOfDouble weights = new MatOfDouble();
    //final Mat target = new Mat(source.height(), source.width(), CvType.CV_8UC1);
    final Mat target = new Mat();
    final Point rectPoint1 = new Point();
    final Point rectPoint2 = new Point();
    final Point fontPoint = new Point();
    protected boolean targetDetected = false;
    protected int contourThickness = 2;
    //protected Scalar contourColor = new Scalar(255, 0, 0); // RED
    protected Scalar contourColor = new Scalar(0, 0, 255);	 // BLUE
    //protected Scalar contourColor = new Scalar(0, 100, 100);	// RED for YUV

    public HumanDetector() {
        hogDescriptor = new HOGDescriptor();
        hogDescriptor.setSVMDetector(HOGDescriptor.getDefaultPeopleDetector());
    }

    public void detect(Image image, OnHumanDetectedListener onHumanDetectedListener) {

        if (!locations.empty()) locations.release();
        if (!weights.empty()) weights.release();
        ByteBuffer buffer;
        byte[] bytes;
        boolean success = false;
        File dir = new File(Environment.getExternalStorageDirectory() + "/aaaaaaaa");
        if (!dir.exists()) {
            dir.mkdir();
        }
        File file = new File(Environment.getExternalStorageDirectory() + "/aaaaaaaa/" + System.currentTimeMillis() + ".jpg");
        FileOutputStream output = null;
        if (image.getFormat() == ImageFormat.JPEG) {
            buffer = image.getPlanes()[0].getBuffer();
            bytes = new byte[buffer.remaining()]; // makes byte array large enough to hold image
            buffer.get(bytes); // copies image from buffer to byte array
            try {
                output = new FileOutputStream(file);
                output.write(bytes);    // write the byte array to file

                Mat source = imread(file.getAbsolutePath());


                Imgproc.cvtColor(source, target, Imgproc.COLOR_BGRA2GRAY);
                hogDescriptor.detectMultiScale(target, locations, weights); //, 0.0, winStride, padding, 1.05, 2.0, false);

        /*
        final Rect[] found = locations.toArray();
        for (int i = 0; i < found.length; i++) {
            Rect rect = found[i];
            for (int j = 0; j < found.length; j++)
                if (j != i && (rect & found[j]) == rect)
                    break;
            if (j == found.size())
                found_filtered.push_back(r);
        }
        */

                // Variables for selection of areas in a photo

                //If there is a result - is added on a photo of area and weight of each of them
                if (locations.rows() > 0) {
                    List<Rect> rectangles = locations.toList();
                    int i = 0;
                    List<Double> weightList = weights.toList();
                    for (final Rect rect : rectangles) {
                        float weight = weightList.get(i++).floatValue();
                        rectPoint1.x = rect.x;
                        rectPoint1.y = rect.y;
                        fontPoint.x = rect.x;
                        fontPoint.y = rect.y - 4;
                        rectPoint2.x = rect.x + rect.width;
                        rectPoint2.y = rect.y + rect.height;
                        // It is added on images the found information
                        Imgproc.rectangle(source, rectPoint1, rectPoint2, contourColor, contourThickness);
                        Imgproc.putText(source, String.format("%1.2f", weight), fontPoint, Core.FONT_HERSHEY_PLAIN, 1.5, contourColor, 2, Core.LINE_AA, false);
                    }
                    targetDetected = true;
                    onHumanDetectedListener.onHumanDetected(source);
                } else {
                    Log.e("LOGGER","NO HUMAN DETECTED");
                    onHumanDetectedListener.onHumanNotDetected(source);
                    targetDetected = false;
                }
                target.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

