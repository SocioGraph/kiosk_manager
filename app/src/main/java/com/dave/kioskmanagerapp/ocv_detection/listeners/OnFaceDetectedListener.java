package com.dave.kioskmanagerapp.ocv_detection.listeners;

import org.opencv.core.Rect;

public interface OnFaceDetectedListener {
    void onFaceDetected(Rect[] faces);
    void onError(String error,int errorCode);
}
