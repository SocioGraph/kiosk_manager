package com.dave.kioskmanagerapp.ocv_detection.listeners;

import org.opencv.core.Rect;

public interface OnFullBodyDetectedListener {
    void onFullBodyDetected(boolean bodyFound , Rect[] faces);
    void onError(String error,int errorCode);
}
