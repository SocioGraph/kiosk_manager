package com.dave.kioskmanagerapp.ocv_detection.listeners;

import org.opencv.core.Mat;

public interface OnHumanDetectedListener {
    void onHumanDetected(Mat humanImage);
    void onHumanNotDetected(Mat humanImage);
}
