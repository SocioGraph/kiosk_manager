package com.dave.kioskmanagerapp.ocv_detection.utils;

import android.content.Context;
import android.util.Log;

import com.dave.kioskmanagerapp.R;

import org.opencv.android.OpenCVLoader;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class OpenCVUtils {
    public enum Classifier{

        FACE_DETECTOR(1),
        FULL_BODY_DETECTOR(2)
        ;

        private int detector;
        /**
         * @param requestDetector
         */
        Classifier(int requestDetector) {
            this.detector = requestDetector;
        }

        public int getDetector() {
            return detector;
        }

    }
    Context mContext = null;
    public OpenCVUtils(Context context) {
        mContext = context;
    }

    public CascadeClassifier getClassifier(Classifier classifier){
        switch (classifier){
            case FACE_DETECTOR:
                return loadClassifierFromRaw("lbpcascade_frontalface.xml",R.raw.lbpcascade_frontalface);
            case FULL_BODY_DETECTOR:
                return loadClassifierFromRaw("haarcascade_fullbody.xml",R.raw.haarcascade_fullbody);

        }
        return null;
    }

    private CascadeClassifier loadClassifierFromRaw(String fileName, int rawResourceId)
    {
        try {
           /* if(OpenCVLoader.initDebug()){
                Log.e("LOGGER","OPEN CV LOADED");
            }else{
                Log.e("LOGGER","OPEN CV NOT LOADED");
            }*/
            InputStream is = mContext.getResources().openRawResource(rawResourceId);
            File cascadeDir = mContext.getDir("cascade", Context.MODE_PRIVATE);
            File mCascadeFile = new File(cascadeDir, fileName);
            FileOutputStream os = new FileOutputStream(mCascadeFile);


            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();
            CascadeClassifier cascadeClassifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());
            return cascadeClassifier;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
