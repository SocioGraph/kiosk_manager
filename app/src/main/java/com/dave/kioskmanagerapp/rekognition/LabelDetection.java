package com.dave.kioskmanagerapp.rekognition;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.format.DateUtils;

import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.model.DetectLabelsRequest;
import com.amazonaws.services.rekognition.model.DetectLabelsResult;
import com.amazonaws.services.rekognition.model.Image;
import com.amazonaws.services.rekognition.model.Label;
import com.dave.kioskmanagerapp.utils.SharedPreference;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;


public class LabelDetection {

    byte[] img = null;

    public void addToQueue(byte[] img,String interactionId, Context mContext){
        try {
            /*DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            File dir = new File(Environment.getExternalStorageDirectory() + "/aaaaaaaa");
            if (!dir.exists()) {
                dir.mkdir();
            }

            String filePath = Environment.getExternalStorageDirectory() + "/aaaaaaaa/" + System.currentTimeMillis() + ".jpg";
            File file = new File(filePath);

            FileOutputStream output = null;

            output = new FileOutputStream(file);
            output.write(img);
            output.close();
            APIQueueTableRowModel apiQueueTableRowModel = new APIQueueTableRowModel("", APIConstants.APIRequestMethod.DETECT_LABEL.name(), filePath, INTERACTIONS_TABLE_NAME, interactionId, DatabaseConstants.APIQueueStatus.PENDING.name(), DatabaseConstants.APIQueuePriority.LOGIN.getValue(), DatabaseConstants.CacheTableType.OBJECTS.getValue());
            databaseManager.insertAPIQueueData(apiQueueTableRowModel);
            DaveService daveService = new DaveService();
            daveService.startBackgroundService(mContext);*/
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    int maxDailyAWSCalls = 1000;
    LabelDetectionListener labelDetectionListener = null;
    String interactionId = "";
    public void detectLabels(Context mContext, byte[] img, String interactionId, LabelDetectionListener labelDetectionListener){
        this.img = img;
        this.interactionId = interactionId;
        this.labelDetectionListener = labelDetectionListener;

        if(!DateUtils.isToday(SharedPreference.getInstance(mContext).getLong("aws_count_time"))){
            SharedPreference.getInstance(mContext).saveLong("aws_count_time",System.currentTimeMillis());
            SharedPreference.getInstance(mContext).saveInt("aws_count",0);
        }

        int currentAwsCallCount = SharedPreference.getInstance(mContext).getInt("aws_count");
        if(currentAwsCallCount++<maxDailyAWSCalls){
            SharedPreference.getInstance(mContext).saveInt("aws_count",currentAwsCallCount);
            new DetectLabelsAsync().execute();
        }else{
            labelDetectionListener.onLabelDetected(new ArrayList<Label>(),interactionId);
        }

    }

    public interface LabelDetectionListener{
        void onLabelDetected(List<Label> labels, String interactionId);
        void onLabelNotDetected(String interactionId);
    }



    class DetectLabelsAsync extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {

            AmazonRekognition rekognitionClient = ClientFactory.createClient();

            DetectLabelsRequest request = new DetectLabelsRequest()
                    .withImage(new Image().withBytes(ByteBuffer.wrap(img)))
                    .withMaxLabels(10).withMinConfidence(75F);

            try {
                DetectLabelsResult result = rekognitionClient.detectLabels(request);
                List<Label> labels = result.getLabels();
                if (labels.size()==0) {
                    if(labelDetectionListener!=null){
                        labelDetectionListener.onLabelNotDetected(interactionId);
                    }
                    System.out.println("  " + "None");
                } else {
                    if(labelDetectionListener!=null){
                        labelDetectionListener.onLabelDetected(labels,interactionId);
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }




}
