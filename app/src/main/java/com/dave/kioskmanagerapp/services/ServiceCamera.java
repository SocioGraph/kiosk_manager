package com.dave.kioskmanagerapp.services;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.Size;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.dave.kioskmanagerapp.R;
import com.dave.kioskmanagerapp.ocv_detection.FaceDetector;
import com.dave.kioskmanagerapp.ocv_detection.FullBodyDetector;
import com.dave.kioskmanagerapp.ocv_detection.HumanDetector;
import com.dave.kioskmanagerapp.ocv_detection.listeners.OnFaceDetectedListener;
import com.dave.kioskmanagerapp.ocv_detection.listeners.OnFullBodyDetectedListener;
import com.dave.kioskmanagerapp.ocv_detection.listeners.OnHumanDetectedListener;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

import static org.opencv.imgcodecs.Imgcodecs.imread;

public class ServiceCamera extends Service {
    protected static final String TAG = "myLog";
    protected static final int CAMERACHOICE = CameraCharacteristics.LENS_FACING_FRONT;
    protected CameraDevice cameraDevice;
    protected CameraCaptureSession session;
    protected ImageReader imageReader;


    //TODO set to false for actual detection
    boolean randomised = true;

    protected CameraDevice.StateCallback cameraStateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            Log.d(TAG, "CameraDevice.StateCallback onOpened");
            cameraDevice = camera;
            actOnReadyCameraDevice();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            Log.w(TAG, "CameraDevice.StateCallback onDisconnected");
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            Log.e(TAG, "CameraDevice.StateCallback onError " + error);
        }
    };

    protected CameraCaptureSession.StateCallback sessionStateCallback = new CameraCaptureSession.StateCallback() {

        @Override
        public void onReady(CameraCaptureSession session) {
            ServiceCamera.this.session = session;
            try {
                session.setRepeatingRequest(createCaptureRequest(), null, null);
//                session.capture(createCaptureRequest(),null,null);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());

                e.printStackTrace();
            }
        }


        @Override
        public void onConfigured(CameraCaptureSession session) {

        }

        @Override
        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
        }
    };
    int counter =0;
    protected ImageReader.OnImageAvailableListener onImageAvailableListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {
            Image img = reader.acquireLatestImage();
            if (img != null) {
                if(counter == 0 || counter == 30) {
                    Log.d(TAG, "onImageAvailable");
                    if(takePic) {
                        /*processImage(img);*/
                        /*FullBodyDetector fullBodyDetector = new FullBodyDetector();
                        fullBodyDetector.processImage(img, ServiceCamera.this, new OnFullBodyDetectedListener() {
                            @Override
                            public void onFullBodyDetected(boolean bodyFound, Rect[] faces) {
                                if(bodyFound) {
                                    Log.e("LOGGER","BODY FOUND !");
                                    FaceDetector faceDetector = new FaceDetector();
                                    faceDetector.processImage(img, ServiceCamera.this, new OnFaceDetectedListener() {
                                        @Override
                                        public void onFaceDetected(Rect[] faces) {
                                            if (faces.length > 0) {
                                                Log.e("LOGGER", "FOUND A FACE");
                                                if (System.currentTimeMillis() - lastFaceFoundTimeStamp > 20000) {
                                                    lastFaceFoundTimeStamp = System.currentTimeMillis();
                                                    sendMyBroadCast("FACE_DETECTED", "", imgBytes);
                                                    Toast.makeText(ServiceCamera.this, "FOUND", Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                Log.e("LOGGER", "NO FACE FOUND");
                                                Toast.makeText(ServiceCamera.this, "NO", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onError(String error, int errorCode) {

                                        }
                                    }, randomised);
                                }else{
                                    Log.e("LOGGER","NO BODY FOUND !");
                                }
                            }

                            @Override
                            public void onError(String error, int errorCode) {

                            }
                        },randomised);*/
                        new HumanDetector().detect(img, new OnHumanDetectedListener() {
                            @Override
                            public void onHumanDetected(Mat tmp) {
                                Log.e("LOGGER","HUMAN DETECTED");
                                Bitmap bmp = Bitmap.createBitmap(tmp.cols(), tmp.rows(), Bitmap.Config.ARGB_8888);
                                Utils.matToBitmap(tmp, bmp);
                                ((ImageView)mFloatingView.findViewById(R.id.img)).setImageBitmap(bmp);
                            }

                            @Override
                            public void onHumanNotDetected(Mat tmp) {
                                Log.e("LOGGER","HUMAN  Not DETECTED");
                                Bitmap bmp = Bitmap.createBitmap(tmp.cols(), tmp.rows(), Bitmap.Config.ARGB_8888);
                                Utils.matToBitmap(tmp, bmp);
                                ((ImageView)mFloatingView.findViewById(R.id.img)).setImageBitmap(bmp);
                            }
                        });

                    }
                    /*session.close();
                    cameraDevice.close();*/
                    counter = 1;
                }
                counter++;
                img.close();
            }

        }
    };
    private void sendMyBroadCast(String broadcastId,String tags, byte[] img)
    {
        try
        {
            Log.e("LOGGER","SENDING TAGS TO SUPER APP = "+tags);
            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(broadcastId);

            broadCastIntent.putExtra("data", tags);
            broadCastIntent.putExtra("img", img);

            sendBroadcast(broadCastIntent);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void readyCamera() {
        CameraManager manager = (CameraManager) getSystemService(CAMERA_SERVICE);
//        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_6, ServiceCamera.this.getApplicationContext(), mLoaderCallback);
        initializeOpenCVDependencies();
        try {
            String pickedCamera = getCamera(manager);
            int width = 0 ;
            int height = 0 ;
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(pickedCamera);

            Size[] jpegSizes = null;

            if (characteristics != null) {
                jpegSizes = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP).getOutputSizes(ImageFormat.JPEG);
            }


            if (jpegSizes != null && 0 < jpegSizes.length) {
                width = jpegSizes[0].getWidth();
                height = jpegSizes[0].getHeight();
                Log.i("jpegSizes width : ", String.valueOf(width));
                Log.i("jpegSizes height : ", String.valueOf(height));
            }

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            manager.openCamera(pickedCamera, cameraStateCallback, null);


            imageReader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 2 /* images buffered */);
            imageReader.setOnImageAvailableListener(onImageAvailableListener, null);
            Log.d(TAG, "imageReader created");
        } catch (Exception e){
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }
    }

    public String getCamera(CameraManager manager){
        try {
            for (String cameraId : manager.getCameraIdList()) {
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
                int cOrientation = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (cOrientation != CAMERACHOICE) {
                    return cameraId;
                }
            }
        } catch (CameraAccessException e){
            e.printStackTrace();
        }
        return null;
    }



    private CascadeClassifier cascadeClassifier;
    private CascadeClassifier cascadeClassifierFullBody;
    private Mat grayscaleImage;
    private int absoluteFaceSize;
    private int absoluteFaceSizeFUllBody;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    initializeOpenCVDependencies();
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };

    private void initializeOpenCVDependencies() {

        try {
            if(OpenCVLoader.initDebug()){
                Log.e("LOGGER","OPEN CV LOADED");
            }else{
                Log.e("LOGGER","OPEN CV NOT LOADED");
            }
            // Copy the resource into a temp file so OpenCV can load it
            InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
            File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
            File mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
            FileOutputStream os = new FileOutputStream(mCascadeFile);


            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();


            InputStream is2 = getResources().openRawResource(R.raw.haarcascade_fullbody);
            File cascadeDir2 = getDir("cascade", Context.MODE_PRIVATE);
            File mCascadeFile2 = new File(cascadeDir2, "haarcascade_fullbody.xml");
            FileOutputStream os2 = new FileOutputStream(mCascadeFile2);


            byte[] buffer2 = new byte[4096];
            int bytesRead2;
            while ((bytesRead2 = is2.read(buffer2)) != -1) {
                os2.write(buffer2, 0, bytesRead2);
            }
            is2.close();
            os2.close();
            // Load the cascade classifier
            cascadeClassifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());
            cascadeClassifierFullBody = new CascadeClassifier(mCascadeFile2.getAbsolutePath());
        } catch (Exception e) {
            Log.e("OpenCVActivity", "Error loading cascade", e);
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand flags " + flags + " startId " + startId);

        readyCamera();

        return super.onStartCommand(intent, flags, startId);
    }


    private WindowManager mWindowManager;
    private View mFloatingView;
    @Override
    public void onCreate() {
        Log.d(TAG,"onCreate service");
        super.onCreate();
        int SDK_INT = Build.VERSION.SDK_INT;
        if (SDK_INT > 7)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            mFloatingView = inflater.inflate(R.layout.overview, null);
            mFloatingView.findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e(TAG,"START CLICKED");
                    takePic = true;
                }
            });
            mFloatingView.findViewById(R.id.stop).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e(TAG,"STOP CLICKED");
                    takePic = false;
                }
            });
            mFloatingView.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e(TAG,"CLOSE CALLED");
                    if(mWindowManager!=null){
                        mWindowManager.removeView(mFloatingView);
                    }
                    ServiceCamera.this.stopSelf();
                }
            });

            int FlagType = 0;
            if (Build.VERSION.SDK_INT >= 26) {
                FlagType = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
            }else{
                FlagType = WindowManager.LayoutParams.TYPE_PHONE;
            }
            final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    FlagType,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);

            //Specify the view position
            params.gravity = Gravity.TOP | Gravity.LEFT;        //Initially view will be added to top-left corner
            params.x = 0;
            params.y = 100;

            //Add the view to the window
            mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
            mWindowManager.addView(mFloatingView, params);

        }
    }

    public void actOnReadyCameraDevice()
    {
        try {
            cameraDevice.createCaptureSession(Arrays.asList(imageReader.getSurface()), sessionStateCallback, null);
        } catch (CameraAccessException e){
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        try {
            session.stopRepeating();
            session.abortCaptures();
        } catch (Exception e){
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }
        if(session!=null)
        session.close();
        Log.e(TAG,"STOP SERVICE CALLED");
        super.onDestroy();
    }

    boolean takePic = false;
    long lastFaceFoundTimeStamp = 0;
    private void processImage(Image image){
        //Process image data
        ByteBuffer buffer;
        byte[] bytes;
        boolean success = false;
        File dir = new File(Environment.getExternalStorageDirectory() + "/aaaaaaaa");
        if(!dir.exists()){
            dir.mkdir();
        }
        File file = new File(Environment.getExternalStorageDirectory() + "/aaaaaaaa/"+System.currentTimeMillis()+".jpg");
        FileOutputStream output = null;

        if(image.getFormat() == ImageFormat.JPEG) {
            buffer = image.getPlanes()[0].getBuffer();
            bytes = new byte[buffer.remaining()]; // makes byte array large enough to hold image
            buffer.get(bytes); // copies image from buffer to byte array
            try {
                output = new FileOutputStream(file);
                output.write(bytes);    // write the byte array to file
                Glide.with(ServiceCamera.this).load(file.getAbsoluteFile()).into(((ImageView)mFloatingView.findViewById(R.id.img)));

//                j++;
                success = true;
                grayscaleImage = new Mat();

                // The faces will be a 20% of the height of the screen
                absoluteFaceSize = (int) (image.getHeight() * 0.2);
                absoluteFaceSizeFUllBody = (int) (image.getHeight() * 0.8);
//                Mat mYuv = new Mat(image.getHeight() , image.getWidth(), CvType.CV_8UC1);
//                mYuv.put(image.getHeight(), image.getWidth(), imgBytes);
                Mat mat = imread(file.getAbsolutePath());
//                Mat mat = Imgcodecs.imdecode(new MatOfByte(bytes), Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);
                Imgproc.cvtColor(mat, grayscaleImage, Imgproc.COLOR_RGBA2RGB);
                MatOfRect faces = new MatOfRect();


                //FULL BODY DETECTION
                /*Log.e("LOGGER","I AM HERE");
                if (cascadeClassifierFullBody != null) {
                    cascadeClassifierFullBody.detectMultiScale(grayscaleImage, faces);
                }
                Log.e("LOGGER","DETECTED HUMAN = "+faces.toArray().length);
                Mat imageMat = Imgcodecs.imread(file.getAbsolutePath());
                for ( Rect rect : faces.toArray())
                {

                    Imgproc.rectangle(imageMat, new Point(rect.x, rect.y),
                            new Point(rect.x + rect.width, rect.y + rect.height),
                            new Scalar(0, 255, 0));
                }
                File file2 = new File(Environment.getExternalStorageDirectory() + "/aaaaaaaa/"+System.currentTimeMillis()+".jpg");

                Imgcodecs.imwrite(file2.getAbsolutePath(), imageMat);
                Glide.with(ServiceCamera.this).load(file2.getAbsoluteFile()).into(((ImageView)mFloatingView.findViewById(R.id.img)));*/
                // END OF FULL BODY DETECTION


                // Use the classifier to detect faces
                if (cascadeClassifier != null) {
                    cascadeClassifier.detectMultiScale(grayscaleImage, faces, 1.1, 2, 2,
                            new org.opencv.core.Size(absoluteFaceSize, absoluteFaceSize), new org.opencv.core.Size());
                }

//                imgBytes = bytes;

                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                int bmpHeight = bmp.getHeight();
                int bmpWidth = bmp.getWidth();
                int divisor = 2;
                Bitmap newBmp = getResizedBitmap(bmp,bmpWidth/divisor,bmpHeight/divisor);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                newBmp.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                imgBytes = stream.toByteArray();
                // If there are any faces found, draw a rectangle around it
                Rect[] facesArray = faces.toArray();
                if(facesArray.length>0){
                    Log.e("LOGGER","FOUND A FACE");
                    if(System.currentTimeMillis()-lastFaceFoundTimeStamp>20000) {
                        lastFaceFoundTimeStamp = System.currentTimeMillis();
                        sendMyBroadCast("FACE_DETECTED", "",imgBytes);
                        Toast.makeText(ServiceCamera.this, "FOUND", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Log.e("LOGGER","NO FACE FOUND");
                    Toast.makeText(ServiceCamera.this,"NO",Toast.LENGTH_SHORT).show();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                image.close(); // close this to free up buffer for other images
                if (null != output) {
                    try {
                        output.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

//            new DetectFace().execute();

        }


    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    byte[] imgBytes;
    boolean foundFace = false;
//    List <FaceDetail> faceDetails = new ArrayList<>();

    protected CaptureRequest createCaptureRequest() {
        try {
            CaptureRequest.Builder builder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
//            CaptureRequest.Builder builder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_VIDEO_SNAPSHOT);
            builder.addTarget(imageReader.getSurface());
            builder.set(CaptureRequest.JPEG_ORIENTATION, 90);
//            builder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
//            builder.set(CaptureRequest.CONTROL_AWB_MODE, CaptureRequest.CONTROL_AWB_MODE_AUTO);
            return builder.build();
        } catch (CameraAccessException e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
