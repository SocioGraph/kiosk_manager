package com.dave.kioskmanagerapp.utils;

public interface AttributeNames {
    public String STORE_ID = "store_id";
    public String STORE_MANAGER_EMAIL = "store_manager_email";
    public String STORE_MANAGER = "store_manager";
    public String DEVICE_ID = "device_id";
    public String USER_ID = "user_id";
    public String DATA = "data";
    public String VIDEO_URL = "video_url";
    public String DEVICE_VIDEO_ID = "device_video_id";
    public String DATE_TO_PLAY = "date_to_play";
    public String VIDEO_ID = "video_id";
    public String CREATED = "created";
    public String APK_URL = "apk_url";
    public String APP_DEVICE_ID = "app_device_id";
    public String APP_ID = "app_id";
    public String COMPANY_PRICE = "spar_price";
    public String ACTUAL_PRICE = "price";
    public String TOTAL_COUNT = "_count";


    public String API_KEY = "api_key";

    public String PRODUCT_DETAILS = "details";
    public String CURRENT_STAGE_DETAILS="current_stage";
    public String NEXT_I_STAGE="next_istage";
    public String RECOM_PRODUCT_DETAILS ="product_attributes";

    public String PRODUCT_NAME ="name";
    public String PRODUCT_IMAGE ="product_image";
    public String PRODUCT_DESCRIPTION ="description";
    public String PRODUCT_BRAND ="brand";
    public String DISCOUNT ="discount";
    public String PRODUCT_CATEGORY_ID ="category_id";
    public String PRODUCT_CATEGORY_NAME ="category_name";
    public String PRODUCT_COLOR ="color_one";


}

