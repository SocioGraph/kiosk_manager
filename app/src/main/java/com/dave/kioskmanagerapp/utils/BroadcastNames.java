package com.dave.kioskmanagerapp.utils;

public enum BroadcastNames {

    VIDEO_PLAYED,
    MEDIA_PLAYING,
    IMAGE_PLAYED,
    TAGS_DETECTED,
    MOTION_DETECTED,
    FACE_DETECTED

}
