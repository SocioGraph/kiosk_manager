package com.dave.kioskmanagerapp.utils;

import com.amazonaws.services.rekognition.model.Label;

import org.json.JSONObject;
import org.opencv.core.Rect;

import java.util.ArrayList;
import java.util.List;

public class DetectedFacesModel {

    Rect face = new Rect();
    int count = 0;
    int countThreshold = 5;
    int frameSkippedThreshold = 10;
    int frameSkipped = 10;
    String customerId = "";
    byte[] image = null;
    List<Label> tags_detected = new ArrayList<>();
    ArrayList<JSONObject> interactions = new ArrayList<>();
    String aws_interaction_id = "";
    boolean isCompleted = false;


    public DetectedFacesModel(Rect face, int countThreshold, int frameSkippedThreshold, byte[] image) {
        this.face = face;
        this.countThreshold = countThreshold;
        this.frameSkippedThreshold = frameSkippedThreshold;
        this.image = image;
    }

    public Rect getFace() {
        return face;
    }

    public void setFace(Rect face) {
        this.face = face;
    }

    public int addCount() {
        return ++count;
    }
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCountThreshold() {
        return countThreshold;
    }

    public void setCountThreshold(int countThreshold) {
        this.countThreshold = countThreshold;
    }

    public int getFrameSkippedThreshold() {
        return frameSkippedThreshold;
    }

    public void setFrameSkippedThreshold(int frameSkippedThreshold) {
        this.frameSkippedThreshold = frameSkippedThreshold;
    }

    public int addFrameSkipped() {
        return ++frameSkipped;
    }
    public int getFrameSkipped() {
        return frameSkipped;
    }

    public void setFrameSkipped(int frameSkipped) {
        this.frameSkipped = frameSkipped;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public List<Label> getTags_detected() {
        return tags_detected;
    }

    public void setTags_detected(List<Label> tags_detected) {
        this.tags_detected = tags_detected;
    }

    public ArrayList<JSONObject> getInteractions() {
        return interactions;
    }

    public void setInteractions(ArrayList<JSONObject> interactions) {
        this.interactions = interactions;
    }

    public String getAws_interaction_id() {
        return aws_interaction_id;
    }

    public void setAws_interaction_id(String aws_interaction_id) {
        this.aws_interaction_id = aws_interaction_id;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }
}
