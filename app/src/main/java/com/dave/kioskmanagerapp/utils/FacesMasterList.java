package com.dave.kioskmanagerapp.utils;

import org.opencv.core.Rect;

import java.util.Random;

public class FacesMasterList {
    Rect face = new Rect();
    int count = 0;
    int countThreshold = 5;
    String customerId = "";
    byte[] image = null;
    String[] customerIds = new String[]{"base_cust_aa1bbe5006394fb9_male_30_40","base_cust_aa1bbe5006394fb9_female_30_40","base_cust_aa1bbe5006394fb9_male_10_20","base_cust_aa1bbe5006394fb9_female_10_20"};
    public FacesMasterList(Rect face,byte[] img) {
        this.face = face;
        this.image = img;
        this.count = 1;
        Random r = new Random();
        this.customerId = customerIds[r.nextInt(customerIds.length)];
    }


    public String getCustomerId() {
        return customerId;
    }

    public boolean addCount(byte[] img) {
        this.image = img;
        count++;
        if(count==countThreshold){
            return true;
        }
        return false;
    }
    public int getCount() {
        return count;
    }

    public void setCount(int count) {

        this.count = count;
    }

    public Rect getFace() {
        return face;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
