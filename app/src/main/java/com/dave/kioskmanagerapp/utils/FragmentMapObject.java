package com.dave.kioskmanagerapp.utils;

import android.support.v4.app.Fragment;

import java.util.HashMap;

public class FragmentMapObject {
    public Fragment fragment;
    public String app_id;
    public String app_device_id;
    public HashMap<String,String> params = new HashMap<>();
    public String app_type;
    public int frameId;
    public FragmentMapObject(Fragment fragment, String app_id, int frameId, String app_device_id, String app_type, HashMap<String,String> params) {
        this.fragment = fragment;
        this.app_id = app_id;
        this.frameId = frameId;
        this.app_device_id = app_device_id;
        this.app_type = app_type;
        this.params = params;
    }
    public FragmentMapObject(Fragment fragment, String app_id, int frameId, String app_device_id, String app_type) {
        this.fragment = fragment;
        this.app_id = app_id;
        this.frameId = frameId;
        this.app_device_id = app_device_id;
        this.app_type = app_type;
        this.params = new HashMap<>();
    }
}
