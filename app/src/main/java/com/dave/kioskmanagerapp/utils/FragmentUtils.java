package com.dave.kioskmanagerapp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class FragmentUtils {
    public void openApp(Context context, Activity activity, String url,int shutOffTime){
        Intent i = new Intent(context,activity.getClass());
        i.putExtra("url",url);
        i.putExtra("shutOffTime",shutOffTime);
        context.startActivity(i);
    }
    public void openOtherApp(Context context, String packageName,int shutOffTime){
        Intent i = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if (i != null) {
            i.putExtra("shutOffTime", shutOffTime);
            context.startActivity(i);
        }
    }
}
