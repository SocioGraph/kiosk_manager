package com.dave.kioskmanagerapp.utils;

import android.content.Context;
import android.widget.Toast;

import com.example.admin.daveai.others.PerspectiveParent;

public class KioskManagerPerspective extends PerspectiveParent {
    //LoginActivity
    long minimumTimeBetweenMotionDetection = 30000;
    int overlapThreshold = 30;
    long available_ping_delay = 10*60*1000;
    int FACE_COUNT_THRESHOLD = 5;
    int FACE_MISSING_FRAME_THRESHOLD = 5;
    public static final String perspectiveName = "kiosk_manager_perspective";

    //MotionDetector
    private long MOTION_DETECTION_FRAME_INTERVAL = 150;
    private int threshPercent = 25;
    private int threshHold = 50;
    private int FACE_DETECT_FPS_DIVIDER = 5;
    private int minBoundingBoxPercent = 1;
    private int maxBoundingBoxPercent = 75;
    private long checkInterval = 500;

    public long getMinimumTimeBetweenMotionDetection() {
        return minimumTimeBetweenMotionDetection;
    }

    public void setMinimumTimeBetweenMotionDetection(long minimumTimeBetweenMotionDetection) {
        this.minimumTimeBetweenMotionDetection = minimumTimeBetweenMotionDetection;
    }

    public int getOverlapThreshold() {
        return overlapThreshold;
    }

    public void setOverlapThreshold(int overlapThreshold) {
        this.overlapThreshold = overlapThreshold;
    }

    public long getAvailable_ping_delay() {
        return available_ping_delay;
    }

    public void setAvailable_ping_delay(long available_ping_delay) {
        this.available_ping_delay = available_ping_delay;
    }

    public int getFACE_COUNT_THRESHOLD() {
        return FACE_COUNT_THRESHOLD;
    }

    public void setFACE_COUNT_THRESHOLD(int FACE_COUNT_THRESHOLD) {
        this.FACE_COUNT_THRESHOLD = FACE_COUNT_THRESHOLD;
    }

    public int getFACE_MISSING_FRAME_THRESHOLD() {
        return FACE_MISSING_FRAME_THRESHOLD;
    }

    public void setFACE_MISSING_FRAME_THRESHOLD(int FACE_MISSING_FRAME_THRESHOLD) {
        this.FACE_MISSING_FRAME_THRESHOLD = FACE_MISSING_FRAME_THRESHOLD;
    }

    public static String getPerspectiveName() {
        return perspectiveName;
    }

    public long getMOTION_DETECTION_FRAME_INTERVAL() {
        return MOTION_DETECTION_FRAME_INTERVAL;
    }

    public void setMOTION_DETECTION_FRAME_INTERVAL(long MOTION_DETECTION_FRAME_INTERVAL) {
        this.MOTION_DETECTION_FRAME_INTERVAL = MOTION_DETECTION_FRAME_INTERVAL;
    }

    public int getThreshPercent() {
        return threshPercent;
    }

    public void setThreshPercent(int threshPercent) {
        this.threshPercent = threshPercent;
    }

    public int getThreshHold() {
        return threshHold;
    }

    public void setThreshHold(int threshHold) {
        this.threshHold = threshHold;
    }

    public int getFACE_DETECT_FPS_DIVIDER() {
        return FACE_DETECT_FPS_DIVIDER;
    }

    public void setFACE_DETECT_FPS_DIVIDER(int FACE_DETECT_FPS_DIVIDER) {
        this.FACE_DETECT_FPS_DIVIDER = FACE_DETECT_FPS_DIVIDER;
    }

    public int getMinBoundingBoxPercent() {
        return minBoundingBoxPercent;
    }

    public void setMinBoundingBoxPercent(int minBoundingBoxPercent) {
        this.minBoundingBoxPercent = minBoundingBoxPercent;
    }

    public int getMaxBoundingBoxPercent() {
        return maxBoundingBoxPercent;
    }

    public void setMaxBoundingBoxPercent(int maxBoundingBoxPercent) {
        this.maxBoundingBoxPercent = maxBoundingBoxPercent;
    }

    public long getCheckInterval() {
        return checkInterval;
    }

    public void setCheckInterval(long checkInterval) {
        this.checkInterval = checkInterval;
    }

    public static final String MINIMUM_TIME_BETWEEN_MOTION_DETECTION_KEY = "minimum_time_between_motion_detection";
    public static final  String OVERLAP_THRESHOLD_KEY = "overlap_threshold";
    public static final  String AVAILABLE_PING_DELAY_KEY = "available_ping_delay";
    public static final  String FACE_COUNT_THRESHOLD_KEY = "face_count_threshold";
    public static final  String FACE_MISSING_FRAME_THRESHOLD_KEY = "face_missing_frame_threshold";
    public static final  String MOTION_DETECTION_FRAME_INTERVAL_KEY = "motion_detection_frame_interval";
    public static final  String THRESH_PERCENT_KEY = "thresh_percent";
    public static final  String BLUR_THRESHOLD_KEY = "blur_threshold";
    public static final  String FACE_DETECT_FPS_DIVIDER_KEY = "face_detect_fps_divider_key";
    public static final  String MIN_BOUNDING_BOX_PERCENT_KEY = "min_bounding_box_percent";
    public static final  String MAX_BOUNDING_BOX_PERCENT_KEY = "max_bounding_box_percent";
    public static final  String CHECK_INTERVAL_KEY = "check_interval";


    private static KioskManagerPerspective singleInstance = null;
    private KioskManagerPerspective() {
    }

    public static KioskManagerPerspective getInstance() {
        if (singleInstance == null) {
            singleInstance = new KioskManagerPerspective();
        }
        return singleInstance;
    }

    public void savePerspectivePreferenceValue(Context context) {
        this.setPreferenceJSONResponse(context, perspectiveName);
        if (this.prefResponse != null && this.prefResponse.length() > 0) {
            try {
                setAttributeValues();
            } catch (Exception var3) {
                var3.printStackTrace();
                Toast.makeText(context, var3.getMessage(), 1).show();
            }
        }

    }


    private void setAttributeValues(){

        this.minimumTimeBetweenMotionDetection = getFromPreference(MINIMUM_TIME_BETWEEN_MOTION_DETECTION_KEY,Number.class,this.minimumTimeBetweenMotionDetection).longValue();
        this.overlapThreshold = getFromPreference(OVERLAP_THRESHOLD_KEY,Number.class,this.overlapThreshold).intValue();
        this.available_ping_delay = getFromPreference(AVAILABLE_PING_DELAY_KEY,Number.class,this.available_ping_delay).longValue();
        this.FACE_COUNT_THRESHOLD = getFromPreference(FACE_COUNT_THRESHOLD_KEY,Number.class,this.FACE_COUNT_THRESHOLD).intValue();
        this.FACE_MISSING_FRAME_THRESHOLD = getFromPreference(FACE_MISSING_FRAME_THRESHOLD_KEY,Number.class,this.FACE_MISSING_FRAME_THRESHOLD).intValue();
        this.MOTION_DETECTION_FRAME_INTERVAL = getFromPreference(MOTION_DETECTION_FRAME_INTERVAL_KEY,Number.class,this.MOTION_DETECTION_FRAME_INTERVAL).longValue();
        this.threshPercent = getFromPreference(THRESH_PERCENT_KEY,Number.class,this.threshPercent).intValue();
        this.threshHold = getFromPreference(BLUR_THRESHOLD_KEY,Number.class,this.threshHold).intValue();
        this.FACE_DETECT_FPS_DIVIDER = getFromPreference(FACE_DETECT_FPS_DIVIDER_KEY,Number.class,this.FACE_DETECT_FPS_DIVIDER).intValue();
        this.minBoundingBoxPercent = getFromPreference(MIN_BOUNDING_BOX_PERCENT_KEY,Number.class,this.minBoundingBoxPercent).intValue();
        this.maxBoundingBoxPercent = getFromPreference(MAX_BOUNDING_BOX_PERCENT_KEY,Number.class,this.maxBoundingBoxPercent).intValue();
        this.checkInterval = getFromPreference(CHECK_INTERVAL_KEY,Number.class,this.checkInterval).longValue();





    }

}
