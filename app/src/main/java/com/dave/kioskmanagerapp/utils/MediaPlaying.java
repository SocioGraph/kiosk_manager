package com.dave.kioskmanagerapp.utils;

public class MediaPlaying {
    String media_id = "";
    String app_id = "";
    String app_device_id = "";

    public MediaPlaying(String media_id, String app_id, String app_device_id) {
        this.media_id = media_id;
        this.app_id = app_id;
        this.app_device_id = app_device_id;
    }

    public String getApp_device_id() {
        return app_device_id;
    }

    public void setApp_device_id(String app_device_id) {
        this.app_device_id = app_device_id;
    }

    public String getMedia_id() {
        return media_id;
    }

    public void setMedia_id(String media_id) {
        this.media_id = media_id;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }
}
