package com.dave.kioskmanagerapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by soham on 20/3/18.
 */

public class SharedPreference {
    private SharedPreferences sharedPrefs;
    private SharedPreferences.Editor editor;
    private String API_KEY = "api_key";
    private String VIEW_ID = "view_id";
    private String BARCODE_URL = "barCodeUrl";
    private String CUSTOMER_ID = "customer_id";
    private String SESSION_ORDER = "session_order";
    private String DEBUG_MODE = "debug_mode";
    private String PRINTER_TARGET = "printer_target";
    private static SharedPreference sSharedPrefs;


    private SharedPreference(Context context) {
        sharedPrefs = context.getSharedPreferences("VisualiserPreference", Context.MODE_PRIVATE);
    }


    public static SharedPreference getInstance(Context context) {
        if (sSharedPrefs == null) {
            sSharedPrefs = new SharedPreference(context.getApplicationContext());
        }
        return sSharedPrefs;
    }

    public static SharedPreference getInstance() {
        if (sSharedPrefs != null) {
            return sSharedPrefs;
        }

        //Option 1:
        throw new IllegalArgumentException("Should use getInstance(Context) at least once before using this method.");

        //Option 2:
        // Alternatively, you can create a new instance here
        // with something like this:
        // getInstance(MyCustomApplication.getAppContext());
    }
/*
    public SharedPreference(Context context){
        sharedPrefs = context.getSharedPreferences("VisualiserPreference", 0);
    }*/


    public void setApiKey(String apiKey){
        editor = sharedPrefs.edit();
        editor.putString(API_KEY,apiKey);
        editor.commit();
    }

    public String getApiKey(){
        return sharedPrefs.getString(API_KEY,"");
    }


    public String getSESSION_ORDER(){
        return sharedPrefs.getString(SESSION_ORDER,"");
    }

    public void setSESSION_ORDER(String session_order){
        editor = sharedPrefs.edit();
        editor.putString(SESSION_ORDER,session_order);
        editor.commit();
    }

  /*  public void setCustomerId(String customerId){
        editor = sharedPrefs.edit();
        editor.putString(CUSTOMER_ID,customerId);
        editor.commit();
    }*/

 /*   public String getCustomerId(){
        return sharedPrefs.getString(CUSTOMER_ID,"");
    }*/

    public void setBarCodeUrl(String barCodeUrl){
        editor = sharedPrefs.edit();
        editor.putString(BARCODE_URL,barCodeUrl);
        editor.commit();
    }

    public String getBarCodeUrl(){
        return sharedPrefs.getString(BARCODE_URL,"");
    }

    public void setViewId(String viewId){
        editor = sharedPrefs.edit();
        editor.putString(VIEW_ID,viewId);
        editor.commit();
    }

    public String getViewId() {
        return sharedPrefs.getString(VIEW_ID, "");
    }

    public void setDEBUG_MODE(Boolean apiKey){
        editor = sharedPrefs.edit();
        editor.putBoolean(DEBUG_MODE,apiKey);
        editor.commit();
    }

    public Boolean getDEBUG_MODE(){
        return sharedPrefs.getBoolean(DEBUG_MODE,false);
    }

    public void setPrintertarget(String printertarget){
        editor = sharedPrefs.edit();
        editor.putString(PRINTER_TARGET,printertarget);
        editor.commit();
    }

    public String getPrintertarget(){
        return sharedPrefs.getString(PRINTER_TARGET,"");
    }


    public void saveString(String keyName, String value){
        editor = sharedPrefs.edit();
        editor.putString(keyName,value);
        editor.commit();
    }

    public String getString(String keyName){
        return sharedPrefs.getString(keyName,"");
    }

    public void saveInt(String keyName, int value){
        editor = sharedPrefs.edit();
        editor.putInt(keyName,value);
        editor.commit();
    }

    public int getInt(String keyName){
        return sharedPrefs.getInt(keyName,0);
    }
    public void saveLong(String keyName, long value){
        editor = sharedPrefs.edit();
        editor.putLong(keyName,value);
        editor.commit();
    }

    public long getLong(String keyName){
        return sharedPrefs.getLong(keyName,0L);
    }


  /*  public void setJsonResponseString(String key,String json){
        editor = sharedPrefs.edit();
        editor.putString(key,json);
        editor.commit();
    }

    public String getJsonResponseString(String key) {
        return sharedPrefs.getString(key,null);
    }*/

    public  void removeKey( String key) {
        editor = sharedPrefs.edit();
        editor.remove(key);
        editor.apply();
    }

    public void clearAllCache(Context cont){
        clearVisualiserCache();
        SharedPreferences.Editor edit = cont.getSharedPreferences("daveAI_sdk",Context.MODE_PRIVATE).edit();
        edit.clear();
        edit.commit();

    }

    public void clearVisualiserCache(){
        editor = sharedPrefs.edit();
        editor.clear();
        editor.commit();
    }
}
