package com.dave.kioskmanagerapp.utils;
import android.view.View;
import android.view.Window;

public class StickyImmersiveFullScreen {

    public static void getFullScreen(Window window){
        //Sticky Immersive FullScreen Flags
        window.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        //Ends Sticky Immersive mode
    }
}
