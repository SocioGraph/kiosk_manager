package com.dave.sociograph.imageviewer.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.bumptech.glide.Glide;
import com.dave.sociograph.imageviewer.R;

import org.json.JSONArray;

import java.util.ArrayList;


/**
 * Created by rudraksh on 12/1/18.
 */

public class ImageAdapter extends PagerAdapter {

    private JSONArray images;
    private LayoutInflater inflater;
    private Context context;

    public ImageAdapter(Context context, JSONArray images) {
        this.context = context;
        this.images=images;
        inflater = LayoutInflater.from(context);
    }

    public void updateImages(JSONArray images){
        this.images=images;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.length();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.pager_image_view, view, false);
        try {
            ImageView myImage = (ImageView) myImageLayout
                    .findViewById(R.id.image);

            Glide.with(context).load(images.getJSONObject(position).getString("media_url")).into(myImage);
            view.addView(myImageLayout, 0);
        }catch (Exception e){
            e.printStackTrace();
        }
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}