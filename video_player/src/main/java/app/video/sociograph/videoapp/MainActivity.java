package app.video.sociograph.videoapp;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.Writer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.HashMap;

public class MainActivity extends Activity {
    private static final int PERMISSION_REQUEST_CODE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= 23)
        {
            if (checkPermission())
            {
                uiInit();
            } else {
                requestPermission(); // Code for permission
            }
        }
        else
        {
            uiInit();
        }


    }



    private boolean checkPermission() {
        Log.e("FLOW--","checkPermission()");
        int result = ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read_res = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int camera_res = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED&&read_res == PackageManager.PERMISSION_GRANTED&&camera_res == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        Log.e("FLOW--","requestPermission()");

        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(MainActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.e("FLOW--","onRequestPermissionsResult()");

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    uiInit();

                    Log.e("value", "Permission Granted, Now you can use local drive .");
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }
    private void sendMyBroadCast(String broadcastId,String video_id)
    {
        try
        {
            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(broadcastId);

            broadCastIntent.putExtra("data", video_id);

            sendBroadcast(broadCastIntent);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    VideoView videoView;
    private void uiInit(){
         videoView=(VideoView)findViewById(R.id.videoView1);
        registerMyReceiver();
        //Creating MediaController
        MediaController mediaController= new MediaController(this);
        mediaController.setAnchorView(videoView);
        //specify the location of media file
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setVolume(0,0);
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Log.e("LOGGER","video is done");
                int lastIndex = 0 ;
                if((currentPlayIndex)==0){
                    lastIndex = playList.length()-1;
                }else{
                    lastIndex = currentPlayIndex-1;
                }
                try {
                    playVideo(playList.getJSONObject(currentPlayIndex).getString("media_url"));
                    sendMyBroadCast("video_played",playList.getJSONObject(currentPlayIndex).toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        try {
            File yourFile = new File(Environment.getExternalStorageDirectory(), "/dave/cached_files/video.json");
            FileInputStream stream = new FileInputStream(yourFile);
            String jsonStr = null;
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

                jsonStr = Charset.defaultCharset().decode(bb).toString();
                JSONObject jobj = new JSONObject(jsonStr);
                playList = jobj.getJSONArray("data");
                if(playList.length()>0)
                playVideo(playList.getJSONObject(0).getString("media_url"));

                fc.close();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                stream.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    JSONArray playList = new JSONArray();
    int currentPlayIndex = 0;
    private void playVideo(String url){
        if((currentPlayIndex+1)==playList.length()){
            currentPlayIndex = 0;
        }else{
            currentPlayIndex++;
        }/*
        File file = new File(url);
        if(file.exists()){
            Log.e("LOGGER","FILE EXISTS");
        }
        Uri uri=Uri.parse(file.getUri());*/

        //Setting MediaController and URI, then starting the videoView
        videoView.setMediaController(null);
        videoView.setVideoPath(url);
        videoView.requestFocus();
        videoView.start();
    }


    MyBroadCastReceiver myBroadCastReceiver;
    private void registerMyReceiver() {

        try
        {
            myBroadCastReceiver = new MyBroadCastReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("refresh_playlist");
            registerReceiver(myBroadCastReceiver, intentFilter);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    class MyBroadCastReceiver extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                Log.e("LOGGER", "Video BROADCAST RECEIVED = " + intent.getAction());
                if (intent.getAction().equals("refresh_playlist")) {
                    try {
                        File yourFile = new File(Environment.getExternalStorageDirectory(), "/dave/cached_files/video.json");
                        FileInputStream stream = new FileInputStream(yourFile);
                        String jsonStr = null;
                        try {
                            FileChannel fc = stream.getChannel();
                            MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

                            jsonStr = Charset.defaultCharset().decode(bb).toString();
                            JSONObject jobj = new JSONObject(jsonStr);
                            playList = jobj.getJSONArray("data");
                            currentPlayIndex = 0;
                            if(videoView.isPlaying()){
                                videoView.stopPlayback();
                            }
                            if(playList.length()>0)
                                playVideo(playList.getJSONObject(0).getString("media_url"));

                            fc.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            stream.close();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }


            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }
    @Override
    protected void onDestroy() {
        Log.e("FLOW--","onDestroy()");

        super.onDestroy();

        unregisterReceiver(myBroadCastReceiver);
    }

}
